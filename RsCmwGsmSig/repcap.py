from enum import Enum
from .Internal.RepeatedCapability import VALUE_DEFAULT
from .Internal.RepeatedCapability import VALUE_EMPTY


# noinspection SpellCheckingInspection
class Instance(Enum):
	"""Global Repeated capability Instance \n
	Selects the instrument"""
	Empty = VALUE_EMPTY
	Default = VALUE_DEFAULT
	Inst1 = 1
	Inst2 = 2
	Inst3 = 3
	Inst4 = 4
	Inst5 = 5
	Inst6 = 6
	Inst7 = 7
	Inst8 = 8
	Inst9 = 9
	Inst10 = 10
	Inst11 = 11
	Inst12 = 12
	Inst13 = 13
	Inst14 = 14
	Inst15 = 15
	Inst16 = 16
	Inst17 = 17
	Inst18 = 18
	Inst19 = 19
	Inst20 = 20
	Inst21 = 21
	Inst22 = 22
	Inst23 = 23
	Inst24 = 24
	Inst25 = 25
	Inst26 = 26
	Inst27 = 27
	Inst28 = 28
	Inst29 = 29
	Inst30 = 30
	Inst31 = 31
	Inst32 = 32
	Inst33 = 33
	Inst34 = 34
	Inst35 = 35
	Inst36 = 36
	Inst37 = 37
	Inst38 = 38
	Inst39 = 39
	Inst40 = 40
	Inst41 = 41
	Inst42 = 42
	Inst43 = 43
	Inst44 = 44
	Inst45 = 45
	Inst46 = 46
	Inst47 = 47
	Inst48 = 48
	Inst49 = 49
	Inst50 = 50
	Inst51 = 51
	Inst52 = 52
	Inst53 = 53
	Inst54 = 54
	Inst55 = 55
	Inst56 = 56
	Inst57 = 57
	Inst58 = 58
	Inst59 = 59
	Inst60 = 60
	Inst61 = 61


# noinspection SpellCheckingInspection
class Carrier(Enum):
	"""Repeated capability Carrier \n
	Carrier"""
	Empty = VALUE_EMPTY
	Default = VALUE_DEFAULT
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class CellNo(Enum):
	"""Repeated capability CellNo \n
	Cell Number"""
	Empty = VALUE_EMPTY
	Default = VALUE_DEFAULT
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4


# noinspection SpellCheckingInspection
class GsmCellNo(Enum):
	"""Repeated capability GsmCellNo \n
	Cell Number"""
	Empty = VALUE_EMPTY
	Default = VALUE_DEFAULT
	Nr1 = 1
	Nr2 = 2
	Nr3 = 3
	Nr4 = 4
	Nr5 = 5
	Nr6 = 6
	Nr7 = 7
	Nr8 = 8
	Nr9 = 9
	Nr10 = 10
	Nr11 = 11
	Nr12 = 12
	Nr13 = 13
	Nr14 = 14
	Nr15 = 15
	Nr16 = 16


# noinspection SpellCheckingInspection
class HsrQAM(Enum):
	"""Repeated capability HsrQAM \n
	Modulation Order"""
	Empty = VALUE_EMPTY
	Default = VALUE_DEFAULT
	QAM16 = 16
	QAM32 = 32


# noinspection SpellCheckingInspection
class IPversion(Enum):
	"""Repeated capability IPversion \n
	IP Version"""
	Empty = VALUE_EMPTY
	Default = VALUE_DEFAULT
	IPv4 = 4
	IPv6 = 6


# noinspection SpellCheckingInspection
class NsrQAM(Enum):
	"""Repeated capability NsrQAM \n
	Modulation Order"""
	Empty = VALUE_EMPTY
	Default = VALUE_DEFAULT
	QAM16 = 16
	QAM32 = 32


# noinspection SpellCheckingInspection
class Output(Enum):
	"""Repeated capability Output \n
	RF Path"""
	Empty = VALUE_EMPTY
	Default = VALUE_DEFAULT
	Nr1 = 1
	Nr2 = 2


# noinspection SpellCheckingInspection
class Path(Enum):
	"""Repeated capability Path \n
	No of Path"""
	Empty = VALUE_EMPTY
	Default = VALUE_DEFAULT
	Nr1 = 1
	Nr2 = 2
