from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal import Conversions
from .....Internal.RepeatedCapability import RepeatedCapability
from ..... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class Output:
	"""Output commands group definition. 1 total commands, 0 Sub-groups, 1 group commands
	Repeated Capability: Output, default value after init: Output.Nr1"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._base = CommandsGroup("output", core, parent)
		self._base.rep_cap = RepeatedCapability(self._base.group_name, 'repcap_output_get', 'repcap_output_set', repcap.Output.Nr1)

	def repcap_output_set(self, enum_value: repcap.Output) -> None:
		"""Repeated Capability default value numeric suffix.
		This value is used, if you do not explicitely set it in the child set/get methods, or if you leave it to Output.Default
		Default value after init: Output.Nr1"""
		self._base.set_repcap_enum_value(enum_value)

	def repcap_output_get(self) -> repcap.Output:
		"""Returns the current default repeated capability for the child set/get methods"""
		# noinspection PyTypeChecker
		return self._base.get_repcap_enum_value()

	def set(self, ext_rf_out_att: float, output=repcap.Output.Default) -> None:
		"""SCPI: CONFigure:GSM:SIGNaling<Instance>:RFSettings:EATTenuation:OUTPut<n> \n
		Snippet: driver.configure.rfSettings.eattenuation.output.set(ext_rf_out_att = 1.0, output = repcap.Output.Default) \n
		Defines an external attenuation (or gain, if the value is negative) , to be applied to the RF output connector. Depending
		on the scenario, several RF output paths are used and the attenuation can be configured per output path. The allowed
		value range can be calculated as follows: Range = [-130 - 'DL Reference Level' to -'DL Reference Level'] \n
			:param ext_rf_out_att: Range: see above , Unit: dB
			:param output: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Output')"""
		param = Conversions.decimal_value_to_str(ext_rf_out_att)
		output_cmd_val = self._base.get_repcap_cmd_value(output, repcap.Output)
		self._core.io.write(f'CONFigure:GSM:SIGNaling<Instance>:RFSettings:EATTenuation:OUTPut{output_cmd_val} {param}')

	def get(self, output=repcap.Output.Default) -> float:
		"""SCPI: CONFigure:GSM:SIGNaling<Instance>:RFSettings:EATTenuation:OUTPut<n> \n
		Snippet: value: float = driver.configure.rfSettings.eattenuation.output.get(output = repcap.Output.Default) \n
		Defines an external attenuation (or gain, if the value is negative) , to be applied to the RF output connector. Depending
		on the scenario, several RF output paths are used and the attenuation can be configured per output path. The allowed
		value range can be calculated as follows: Range = [-130 - 'DL Reference Level' to -'DL Reference Level'] \n
			:param output: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Output')
			:return: ext_rf_out_att: Range: see above , Unit: dB"""
		output_cmd_val = self._base.get_repcap_cmd_value(output, repcap.Output)
		response = self._core.io.query_str(f'CONFigure:GSM:SIGNaling<Instance>:RFSettings:EATTenuation:OUTPut{output_cmd_val}?')
		return Conversions.str_to_float(response)

	def clone(self) -> 'Output':
		"""Clones the group by creating new object from it and its whole existing sub-groups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = Output(self._core, self._base.parent)
		self._base.synchronize_repcaps(new_group)
		return new_group
