from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class DualBand:
	"""DualBand commands group definition. 2 total commands, 2 Sub-groups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._base = CommandsGroup("dualBand", core, parent)

	@property
	def band(self):
		"""band commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_band'):
			from .DualBand_.Band import Band
			self._band = Band(self._core, self._base)
		return self._band

	@property
	def combined(self):
		"""combined commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_combined'):
			from .DualBand_.Combined import Combined
			self._combined = Combined(self._core, self._base)
		return self._combined

	def clone(self) -> 'DualBand':
		"""Clones the group by creating new object from it and its whole existing sub-groups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = DualBand(self._core, self._base.parent)
		self._base.synchronize_repcaps(new_group)
		return new_group
