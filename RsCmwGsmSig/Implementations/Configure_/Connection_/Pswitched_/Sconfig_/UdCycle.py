from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class UdCycle:
	"""UdCycle commands group definition. 2 total commands, 1 Sub-groups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._base = CommandsGroup("udCycle", core, parent)

	@property
	def downlink(self):
		"""downlink commands group. 0 Sub-classes, 2 commands."""
		if not hasattr(self, '_downlink'):
			from .UdCycle_.Downlink import Downlink
			self._downlink = Downlink(self._core, self._base)
		return self._downlink

	def clone(self) -> 'UdCycle':
		"""Clones the group by creating new object from it and its whole existing sub-groups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = UdCycle(self._core, self._base.parent)
		self._base.synchronize_repcaps(new_group)
		return new_group
