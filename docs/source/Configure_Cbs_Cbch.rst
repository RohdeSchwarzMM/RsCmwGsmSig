Cbch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CBS:CBCH:ENABle

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CBS:CBCH:ENABle



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cbs_.Cbch.Cbch
	:members:
	:undoc-members:
	:noindex: