Tch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:PCL:TCH:CSWitched

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:RFSettings:PCL:TCH:CSWitched



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.Pcl_.Tch.Tch
	:members:
	:undoc-members:
	:noindex: