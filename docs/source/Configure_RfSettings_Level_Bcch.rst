Bcch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:LEVel:BCCH

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:RFSettings:LEVel:BCCH



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.Level_.Bcch.Bcch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.level.bcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Level_Bcch_Minimum.rst