Frate
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Cswitched_.Amr_.Cmode_.Wb_.Frate.Frate
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.cswitched.amr.cmode.wb.frate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Cswitched_Amr_Cmode_Wb_Frate_Gmsk.rst
	Configure_Connection_Cswitched_Amr_Cmode_Wb_Frate_Epsk.rst