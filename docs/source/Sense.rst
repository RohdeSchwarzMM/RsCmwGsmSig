Sense
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:CVINfo

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:CVINfo



.. autoclass:: RsCmwGsmSig.Implementations.Sense.Sense
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Band.rst
	Sense_IqOut.rst
	Sense_Connection.rst
	Sense_MssInfo.rst
	Sense_Cell.rst
	Sense_Rreport.rst
	Sense_Sms.rst
	Sense_Ber.rst
	Sense_RfSettings.rst
	Sense_Elog.rst