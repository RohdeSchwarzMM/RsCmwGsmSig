Scell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:GSM:SIGNaling<Instance>:SCENario:SCELl:FLEXible

.. code-block:: python

	ROUTe:GSM:SIGNaling<Instance>:SCENario:SCELl:FLEXible



.. autoclass:: RsCmwGsmSig.Implementations.Route_.Scenario_.Scell.Scell
	:members:
	:undoc-members:
	:noindex: