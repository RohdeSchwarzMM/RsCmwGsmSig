Wb
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Sense_.MssInfo_.Amr_.Cmode_.Wb.Wb
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.mssInfo.amr.cmode.wb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_MssInfo_Amr_Cmode_Wb_Frate.rst
	Sense_MssInfo_Amr_Cmode_Wb_Hrate.rst