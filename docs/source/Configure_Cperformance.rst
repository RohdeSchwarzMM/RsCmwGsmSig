Cperformance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CPERformance:TOUT
	single: CONFigure:GSM:SIGNaling<Instance>:CPERformance:TLEVel

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CPERformance:TOUT
	CONFigure:GSM:SIGNaling<Instance>:CPERformance:TLEVel



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cperformance.Cperformance
	:members:
	:undoc-members:
	:noindex: