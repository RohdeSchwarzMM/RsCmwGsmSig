ReSelection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:RESelection:TRESelection
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:RESelection:HYSTeresis

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CELL:RESelection:TRESelection
	CONFigure:GSM:SIGNaling<Instance>:CELL:RESelection:HYSTeresis



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cell_.ReSelection.ReSelection
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.reSelection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_ReSelection_Quality.rst