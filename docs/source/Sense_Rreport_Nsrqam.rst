Nsrqam<NsrQAM>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: QAM16 .. QAM32
	rc = driver.sense.rreport.nsrqam.repcap_nsrQAM_get()
	driver.sense.rreport.nsrqam.repcap_nsrQAM_set(repcap.NsrQAM.QAM16)





.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Rreport_.Nsrqam.Nsrqam
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.rreport.nsrqam.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Rreport_Nsrqam_Mbep.rst
	Sense_Rreport_Nsrqam_Cbep.rst