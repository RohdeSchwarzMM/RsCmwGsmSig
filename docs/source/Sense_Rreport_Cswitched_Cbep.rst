Cbep
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:RREPort:CSWitched:CBEP:RANGe
	single: SENSe:GSM:SIGNaling<Instance>:RREPort:CSWitched:CBEP

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:RREPort:CSWitched:CBEP:RANGe
	SENSe:GSM:SIGNaling<Instance>:RREPort:CSWitched:CBEP



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Rreport_.Cswitched_.Cbep.Cbep
	:members:
	:undoc-members:
	:noindex: