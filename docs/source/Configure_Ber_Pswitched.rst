Pswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:BER:PSWitched:TOUT
	single: CONFigure:GSM:SIGNaling<Instance>:BER:PSWitched:MMODe
	single: CONFigure:GSM:SIGNaling<Instance>:BER:PSWitched:SCONdition
	single: CONFigure:GSM:SIGNaling<Instance>:BER:PSWitched:SCOunt

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:BER:PSWitched:TOUT
	CONFigure:GSM:SIGNaling<Instance>:BER:PSWitched:MMODe
	CONFigure:GSM:SIGNaling<Instance>:BER:PSWitched:SCONdition
	CONFigure:GSM:SIGNaling<Instance>:BER:PSWitched:SCOunt



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Ber_.Pswitched.Pswitched
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ber.pswitched.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ber_Pswitched_Limit.rst