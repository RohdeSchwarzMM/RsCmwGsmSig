LrMessage
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:SMS:INFO:LRMessage:RFLag

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:SMS:INFO:LRMessage:RFLag



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Sms_.Info_.LrMessage.LrMessage
	:members:
	:undoc-members:
	:noindex: