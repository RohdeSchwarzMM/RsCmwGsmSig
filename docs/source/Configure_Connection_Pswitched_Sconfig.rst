Sconfig
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Pswitched_.Sconfig.Sconfig
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pswitched.sconfig.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pswitched_Sconfig_Combined.rst
	Configure_Connection_Pswitched_Sconfig_Enable.rst
	Configure_Connection_Pswitched_Sconfig_Gamma.rst
	Configure_Connection_Pswitched_Sconfig_Level.rst
	Configure_Connection_Pswitched_Sconfig_Cscheme.rst
	Configure_Connection_Pswitched_Sconfig_UdCycle.rst