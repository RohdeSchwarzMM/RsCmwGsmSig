Loss
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:LOSS:USER
	single: CONFigure:GSM:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:LOSS:NORMal

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:LOSS:USER
	CONFigure:GSM:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:LOSS:NORMal



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Fading_.Fsimulator_.Iloss_.Loss.Loss
	:members:
	:undoc-members:
	:noindex: