Frate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:THReshold:WB:FRATe:GMSK
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:THReshold:WB:FRATe:EPSK

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:THReshold:WB:FRATe:GMSK
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:THReshold:WB:FRATe:EPSK



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Cswitched_.Amr_.Threshold_.Wb_.Frate.Frate
	:members:
	:undoc-members:
	:noindex: