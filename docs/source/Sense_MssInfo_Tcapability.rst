Tcapability
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:TCAPability:SSCHannels
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:TCAPability:GEGPrs
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:TCAPability:ETWO

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:MSSinfo:TCAPability:SSCHannels
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:TCAPability:GEGPrs
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:TCAPability:ETWO



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.MssInfo_.Tcapability.Tcapability
	:members:
	:undoc-members:
	:noindex: