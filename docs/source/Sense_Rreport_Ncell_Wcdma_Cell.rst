Cell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:RREPort:NCELl:WCDMa:CELL<CellNo>

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:RREPort:NCELl:WCDMa:CELL<CellNo>



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Rreport_.Ncell_.Wcdma_.Cell.Cell
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.rreport.ncell.wcdma.cell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Rreport_Ncell_Wcdma_Cell_Range.rst