Ncc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:NCC:PERMitted
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:NCC

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CELL:NCC:PERMitted
	CONFigure:GSM:SIGNaling<Instance>:CELL:NCC



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cell_.Ncc.Ncc
	:members:
	:undoc-members:
	:noindex: