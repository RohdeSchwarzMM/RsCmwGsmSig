Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:GSM:SIGNaling<Instance>:BER:PSWitched:CARRier<Const_Carrier>
	single: FETCh:GSM:SIGNaling<Instance>:BER:PSWitched:CARRier<Const_Carrier>

.. code-block:: python

	READ:GSM:SIGNaling<Instance>:BER:PSWitched:CARRier<Const_Carrier>
	FETCh:GSM:SIGNaling<Instance>:BER:PSWitched:CARRier<Const_Carrier>



.. autoclass:: RsCmwGsmSig.Implementations.Ber_.Pswitched_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex: