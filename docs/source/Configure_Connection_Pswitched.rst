Pswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:SERVice
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:DSOurce
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:TLEVel
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:EDALlocation
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:NOPDus
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:SOFFset
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:CATYpe
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:BPERiod<Const_Bperiod>
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:BDCRate
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:ASRDblocks
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:IREDundancy

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:SERVice
	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:DSOurce
	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:TLEVel
	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:EDALlocation
	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:NOPDus
	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:SOFFset
	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:CATYpe
	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:BPERiod<Const_Bperiod>
	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:BDCRate
	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:ASRDblocks
	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:IREDundancy



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Pswitched.Pswitched
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pswitched.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pswitched_Sconfig.rst
	Configure_Connection_Pswitched_DpControl.rst
	Configure_Connection_Pswitched_Cscheme.rst
	Configure_Connection_Pswitched_DldCarrier.rst