Ber
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Ber.Ber
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ber.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ber_Cswitched.rst
	Ber_Pswitched.rst