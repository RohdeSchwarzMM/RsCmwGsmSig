Route
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:GSM:SIGNaling<Instance>

.. code-block:: python

	ROUTe:GSM:SIGNaling<Instance>



.. autoclass:: RsCmwGsmSig.Implementations.Route.Route
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario.rst