RsCmwGsmSig API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCmwGsmSig('TCPIP::192.168.2.101::HISLIP')
	# Instance range: Inst1 .. Inst61
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCmwGsmSig.RsCmwGsmSig
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Route.rst
	Configure.rst
	Sense.rst
	Clean.rst
	Source.rst
	Call.rst
	Cswitched.rst
	Pswitched.rst
	Prepare.rst
	Handover.rst
	Ber.rst
	Intermediate.rst
	Bler.rst
	Throughput.rst
	Cperformance.rst