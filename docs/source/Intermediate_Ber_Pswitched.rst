Pswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:INTermediate:GSM:SIGNaling<Instance>:BER:PSWitched

.. code-block:: python

	FETCh:INTermediate:GSM:SIGNaling<Instance>:BER:PSWitched



.. autoclass:: RsCmwGsmSig.Implementations.Intermediate_.Ber_.Pswitched.Pswitched
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.intermediate.ber.pswitched.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Intermediate_Ber_Pswitched_Mbep.rst