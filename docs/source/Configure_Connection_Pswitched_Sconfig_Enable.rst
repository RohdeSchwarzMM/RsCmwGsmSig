Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:SCONfig:ENABle:UL

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:SCONfig:ENABle:UL



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Pswitched_.Sconfig_.Enable.Enable
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pswitched.sconfig.enable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pswitched_Sconfig_Enable_Downlink.rst