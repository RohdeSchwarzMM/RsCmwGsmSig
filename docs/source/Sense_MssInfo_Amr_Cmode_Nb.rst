Nb
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Sense_.MssInfo_.Amr_.Cmode_.Nb.Nb
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.mssInfo.amr.cmode.nb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_MssInfo_Amr_Cmode_Nb_Frate.rst
	Sense_MssInfo_Amr_Cmode_Nb_Hrate.rst