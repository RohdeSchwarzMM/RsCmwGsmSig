DldCarrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:DLDCarrier:ENABle

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:DLDCarrier:ENABle



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Pswitched_.DldCarrier.DldCarrier
	:members:
	:undoc-members:
	:noindex: