Output<Output>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.rfSettings.eattenuation.output.repcap_output_get()
	driver.configure.rfSettings.eattenuation.output.repcap_output_set(repcap.Output.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:EATTenuation:OUTPut<Output>

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:RFSettings:EATTenuation:OUTPut<Output>



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.Eattenuation_.Output.Output
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.eattenuation.output.clone()