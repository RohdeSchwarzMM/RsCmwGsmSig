Gamma
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:SCONfig:GAMMa:UL

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:SCONfig:GAMMa:UL



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Pswitched_.Sconfig_.Gamma.Gamma
	:members:
	:undoc-members:
	:noindex: