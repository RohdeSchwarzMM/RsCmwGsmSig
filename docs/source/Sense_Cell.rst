Cell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:CELL:FNUMber
	single: SENSe:GSM:SIGNaling<Instance>:CELL:CERRor

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:CELL:FNUMber
	SENSe:GSM:SIGNaling<Instance>:CELL:CERRor



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Cell.Cell
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.cell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Cell_Pswitched.rst