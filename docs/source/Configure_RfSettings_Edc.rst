Edc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:EDC:OUTPut
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:EDC:INPut

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:RFSettings:EDC:OUTPut
	CONFigure:GSM:SIGNaling<Instance>:RFSettings:EDC:INPut



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.Edc.Edc
	:members:
	:undoc-members:
	:noindex: