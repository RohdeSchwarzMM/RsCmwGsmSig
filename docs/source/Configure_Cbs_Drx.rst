Drx
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CBS:DRX:ENABle
	single: CONFigure:GSM:SIGNaling<Instance>:CBS:DRX:LENGth
	single: CONFigure:GSM:SIGNaling<Instance>:CBS:DRX:OFFSet

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CBS:DRX:ENABle
	CONFigure:GSM:SIGNaling<Instance>:CBS:DRX:LENGth
	CONFigure:GSM:SIGNaling<Instance>:CBS:DRX:OFFSet



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cbs_.Drx.Drx
	:members:
	:undoc-members:
	:noindex: