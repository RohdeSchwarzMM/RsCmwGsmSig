Message
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:ENABle
	single: CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:ID
	single: CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:IDTYpe
	single: CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:SERial
	single: CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:DCSCheme
	single: CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:CATegory
	single: CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:DATA
	single: CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:PERiod

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:ENABle
	CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:ID
	CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:IDTYpe
	CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:SERial
	CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:DCSCheme
	CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:CATegory
	CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:DATA
	CONFigure:GSM:SIGNaling<Instance>:CBS:MESSage:PERiod



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cbs_.Message.Message
	:members:
	:undoc-members:
	:noindex: