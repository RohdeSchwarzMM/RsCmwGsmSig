Connection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:ASConfig
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:DSConfig
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:TADVance
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:RFOFfset

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:ASConfig
	CONFigure:GSM:SIGNaling<Instance>:CONNection:DSConfig
	CONFigure:GSM:SIGNaling<Instance>:CONNection:TADVance
	CONFigure:GSM:SIGNaling<Instance>:CONNection:RFOFfset



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection.Connection
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Cswitched.rst
	Configure_Connection_Pswitched.rst
	Configure_Connection_Foffset.rst