DualBand
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Configure_.DualBand.DualBand
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.dualBand.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_DualBand_Band.rst
	Configure_DualBand_Combined.rst