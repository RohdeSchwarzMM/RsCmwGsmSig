Cswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:GSM:SIGNaling<Instance>:CSWitched:ACTion

.. code-block:: python

	CALL:GSM:SIGNaling<Instance>:CSWitched:ACTion



.. autoclass:: RsCmwGsmSig.Implementations.Call_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex: