Foffset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:FOFFset:UL
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:FOFFset:DL

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:FOFFset:UL
	CONFigure:GSM:SIGNaling<Instance>:CONNection:FOFFset:DL



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Foffset.Foffset
	:members:
	:undoc-members:
	:noindex: