DpControl
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:DPControl:ENABle
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:DPControl:P
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:DPControl:PMODe
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:DPControl:PFIeld

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:DPControl:ENABle
	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:DPControl:P
	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:DPControl:PMODe
	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:DPControl:PFIeld



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Pswitched_.DpControl.DpControl
	:members:
	:undoc-members:
	:noindex: