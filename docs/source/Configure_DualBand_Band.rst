Band
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:DUALband:BAND:TCH

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:DUALband:BAND:TCH



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.DualBand_.Band.Band
	:members:
	:undoc-members:
	:noindex: