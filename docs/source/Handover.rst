Handover
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Handover.Handover
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.handover.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Handover_State.rst