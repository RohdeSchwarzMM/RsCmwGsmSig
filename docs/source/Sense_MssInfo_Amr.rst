Amr
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Sense_.MssInfo_.Amr.Amr
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.mssInfo.amr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_MssInfo_Amr_Cmode.rst