Cswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:BER:CSWitched:RTDelay

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:BER:CSWitched:RTDelay



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Ber_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex: