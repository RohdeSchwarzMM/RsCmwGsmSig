Cswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:TSLot
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:TMODe
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:HRSubchannel
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:DSOurce
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:CRELease
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:EDELay
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:LOOP
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:LREClose
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:CID
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:TCHassign
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:RFACch
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:RSACch

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:TSLot
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:TMODe
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:HRSubchannel
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:DSOurce
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:CRELease
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:EDELay
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:LOOP
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:LREClose
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:CID
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:TCHassign
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:RFACch
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:RSACch



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.cswitched.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Cswitched_Dtx.rst
	Configure_Connection_Cswitched_Amr.rst
	Configure_Connection_Cswitched_Vamos.rst