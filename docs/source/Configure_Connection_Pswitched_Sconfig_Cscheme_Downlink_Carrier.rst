Carrier<Carrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.connection.pswitched.sconfig.cscheme.downlink.carrier.repcap_carrier_get()
	driver.configure.connection.pswitched.sconfig.cscheme.downlink.carrier.repcap_carrier_set(repcap.Carrier.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:SCONfig:CSCHeme:DL:CARRier<Carrier>

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:SCONfig:CSCHeme:DL:CARRier<Carrier>



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Pswitched_.Sconfig_.Cscheme_.Downlink_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pswitched.sconfig.cscheme.downlink.carrier.clone()