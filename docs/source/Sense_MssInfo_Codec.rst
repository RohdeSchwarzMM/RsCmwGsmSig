Codec
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:CODec:GSM
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:CODec:UMTS

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:MSSinfo:CODec:GSM
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:CODec:UMTS



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.MssInfo_.Codec.Codec
	:members:
	:undoc-members:
	:noindex: