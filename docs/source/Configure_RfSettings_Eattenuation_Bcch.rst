Bcch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:EATTenuation:BCCH:OUTPut

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:RFSettings:EATTenuation:BCCH:OUTPut



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.Eattenuation_.Bcch.Bcch
	:members:
	:undoc-members:
	:noindex: