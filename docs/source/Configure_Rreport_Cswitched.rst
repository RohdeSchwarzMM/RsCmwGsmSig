Cswitched
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Rreport_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rreport.cswitched.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Rreport_Cswitched_EmReport.rst