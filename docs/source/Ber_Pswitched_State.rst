State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:SIGNaling<Instance>:BER:PSWitched:STATe

.. code-block:: python

	FETCh:GSM:SIGNaling<Instance>:BER:PSWitched:STATe



.. autoclass:: RsCmwGsmSig.Implementations.Ber_.Pswitched_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ber.pswitched.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ber_Pswitched_State_All.rst