Cswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:INTermediate:GSM:SIGNaling<Instance>:BER:CSWitched

.. code-block:: python

	FETCh:INTermediate:GSM:SIGNaling<Instance>:BER:CSWitched



.. autoclass:: RsCmwGsmSig.Implementations.Intermediate_.Ber_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.intermediate.ber.cswitched.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Intermediate_Ber_Cswitched_Mbep.rst