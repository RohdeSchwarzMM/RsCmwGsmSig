State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:SIGNaling<Instance>:BLER:STATe

.. code-block:: python

	FETCh:GSM:SIGNaling<Instance>:BLER:STATe



.. autoclass:: RsCmwGsmSig.Implementations.Bler_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bler.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Bler_State_All.rst