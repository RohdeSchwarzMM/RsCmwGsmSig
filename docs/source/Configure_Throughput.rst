Throughput
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:THRoughput:TOUT
	single: CONFigure:GSM:SIGNaling<Instance>:THRoughput:WINDow
	single: CONFigure:GSM:SIGNaling<Instance>:THRoughput:REPetition

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:THRoughput:TOUT
	CONFigure:GSM:SIGNaling<Instance>:THRoughput:WINDow
	CONFigure:GSM:SIGNaling<Instance>:THRoughput:REPetition



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Throughput.Throughput
	:members:
	:undoc-members:
	:noindex: