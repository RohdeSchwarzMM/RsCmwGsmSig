Cswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:TOUT
	single: CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:MMODe
	single: CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:SCONdition
	single: CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:SCOunt
	single: CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:RTDelay

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:TOUT
	CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:MMODe
	CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:SCONdition
	CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:SCOunt
	CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:RTDelay



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Ber_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ber.cswitched.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ber_Cswitched_Limit.rst