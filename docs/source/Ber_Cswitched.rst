Cswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:GSM:SIGNaling<Instance>:BER:CSWitched
	single: STOP:GSM:SIGNaling<Instance>:BER:CSWitched
	single: ABORt:GSM:SIGNaling<Instance>:BER:CSWitched
	single: READ:GSM:SIGNaling<Instance>:BER:CSWitched
	single: FETCh:GSM:SIGNaling<Instance>:BER:CSWitched

.. code-block:: python

	INITiate:GSM:SIGNaling<Instance>:BER:CSWitched
	STOP:GSM:SIGNaling<Instance>:BER:CSWitched
	ABORt:GSM:SIGNaling<Instance>:BER:CSWitched
	READ:GSM:SIGNaling<Instance>:BER:CSWitched
	FETCh:GSM:SIGNaling<Instance>:BER:CSWitched



.. autoclass:: RsCmwGsmSig.Implementations.Ber_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ber.cswitched.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ber_Cswitched_State.rst