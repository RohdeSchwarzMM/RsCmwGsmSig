Sync
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:SYNC:ZONE
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:SYNC:OFFSet

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CELL:SYNC:ZONE
	CONFigure:GSM:SIGNaling<Instance>:CELL:SYNC:OFFSet



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cell_.Sync.Sync
	:members:
	:undoc-members:
	:noindex: