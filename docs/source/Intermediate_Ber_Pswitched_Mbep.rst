Mbep
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:INTermediate:GSM:SIGNaling<Instance>:BER:PSWitched:MBEP

.. code-block:: python

	FETCh:INTermediate:GSM:SIGNaling<Instance>:BER:PSWitched:MBEP



.. autoclass:: RsCmwGsmSig.Implementations.Intermediate_.Ber_.Pswitched_.Mbep.Mbep
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.intermediate.ber.pswitched.mbep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Intermediate_Ber_Pswitched_Mbep_Enhanced.rst