Downlink
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:SCONfig:UDCYcle:DL:CARRier<Const_Carrier>
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:SCONfig:UDCYcle:DL

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:SCONfig:UDCYcle:DL:CARRier<Const_Carrier>
	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:SCONfig:UDCYcle:DL



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Pswitched_.Sconfig_.UdCycle_.Downlink.Downlink
	:members:
	:undoc-members:
	:noindex: