Connection
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Connection.Connection
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Cswitched.rst
	Sense_Connection_Ethroughput.rst