Pswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:CELL:PSWitched:CERRor

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:CELL:PSWitched:CERRor



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Cell_.Pswitched.Pswitched
	:members:
	:undoc-members:
	:noindex: