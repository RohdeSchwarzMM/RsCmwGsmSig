Outgoing
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:SDOMain
	single: CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:INTernal
	single: CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:BINary
	single: CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:DCODing
	single: CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:CGRoup
	single: CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:MCLass
	single: CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:OSADdress
	single: CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:OADDress
	single: CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:UDHeader
	single: CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:PIDentifier

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:SDOMain
	CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:INTernal
	CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:BINary
	CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:DCODing
	CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:CGRoup
	CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:MCLass
	CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:OSADdress
	CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:OADDress
	CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:UDHeader
	CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:PIDentifier



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Sms_.Outgoing.Outgoing
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sms.outgoing.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sms_Outgoing_SctStamp.rst