Channel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:CHANnel:BCCH

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:RFSettings:CHANnel:BCCH



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.Channel.Channel
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.channel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Channel_Tch.rst