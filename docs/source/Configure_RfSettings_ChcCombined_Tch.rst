Tch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:CHCCombined:TCH:CSWitched

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:RFSettings:CHCCombined:TCH:CSWitched



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.ChcCombined_.Tch.Tch
	:members:
	:undoc-members:
	:noindex: