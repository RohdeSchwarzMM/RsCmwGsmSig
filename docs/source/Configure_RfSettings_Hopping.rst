Hopping
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.Hopping.Hopping
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.hopping.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Hopping_Enable.rst
	Configure_RfSettings_Hopping_Sequence.rst
	Configure_RfSettings_Hopping_Hsn.rst
	Configure_RfSettings_Hopping_Maio.rst