Cswitched
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Connection_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.cswitched.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Cswitched_Connection.rst