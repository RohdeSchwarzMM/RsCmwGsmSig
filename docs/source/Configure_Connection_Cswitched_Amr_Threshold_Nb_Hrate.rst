Hrate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:THReshold:NB:HRATe:GMSK
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:THReshold:NB:HRATe:EPSK

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:THReshold:NB:HRATe:GMSK
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:THReshold:NB:HRATe:EPSK



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Cswitched_.Amr_.Threshold_.Nb_.Hrate.Hrate
	:members:
	:undoc-members:
	:noindex: