Cell<GsmCellNo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr16
	rc = driver.configure.ncell.gsm.cell.repcap_gsmCellNo_get()
	driver.configure.ncell.gsm.cell.repcap_gsmCellNo_set(repcap.GsmCellNo.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:NCELl:GSM:CELL<GsmCellNo>

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:NCELl:GSM:CELL<GsmCellNo>



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Ncell_.Gsm_.Cell.Cell
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ncell.gsm.cell.clone()