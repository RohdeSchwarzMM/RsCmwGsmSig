Epsk
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:CMODe:WB:HRATe:EPSK:DL
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:CMODe:WB:HRATe:EPSK:UL

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:CMODe:WB:HRATe:EPSK:DL
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:CMODe:WB:HRATe:EPSK:UL



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Cswitched_.Amr_.Cmode_.Wb_.Hrate_.Epsk.Epsk
	:members:
	:undoc-members:
	:noindex: