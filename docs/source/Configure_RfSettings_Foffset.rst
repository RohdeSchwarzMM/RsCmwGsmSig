Foffset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:FOFFset:DL
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:FOFFset:UL

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:RFSettings:FOFFset:DL
	CONFigure:GSM:SIGNaling<Instance>:RFSettings:FOFFset:UL



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.Foffset.Foffset
	:members:
	:undoc-members:
	:noindex: