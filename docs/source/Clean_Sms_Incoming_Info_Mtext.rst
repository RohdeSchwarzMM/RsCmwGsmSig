Mtext
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CLEan:GSM:SIGNaling<Instance>:SMS:INComing:INFO:MTEXt

.. code-block:: python

	CLEan:GSM:SIGNaling<Instance>:SMS:INComing:INFO:MTEXt



.. autoclass:: RsCmwGsmSig.Implementations.Clean_.Sms_.Incoming_.Info_.Mtext.Mtext
	:members:
	:undoc-members:
	:noindex: