Bler
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:BLER:TOUT
	single: CONFigure:GSM:SIGNaling<Instance>:BLER:SCOunt

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:BLER:TOUT
	CONFigure:GSM:SIGNaling<Instance>:BLER:SCOunt



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Bler.Bler
	:members:
	:undoc-members:
	:noindex: