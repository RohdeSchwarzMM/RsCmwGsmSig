Amr
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Cswitched_.Amr.Amr
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.cswitched.amr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Cswitched_Amr_Signaling.rst
	Configure_Connection_Cswitched_Amr_Rset.rst
	Configure_Connection_Cswitched_Amr_Cmode.rst
	Configure_Connection_Cswitched_Amr_Threshold.rst