Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:CURRent
	single: READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:CURRent

.. code-block:: python

	FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:CURRent
	READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:CURRent



.. autoclass:: RsCmwGsmSig.Implementations.Throughput_.Trace_.Uplink_.Pdu_.Current.Current
	:members:
	:undoc-members:
	:noindex: