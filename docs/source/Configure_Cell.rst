Cell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:PSDomain
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:NSUPport
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:ECIot
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:DTMode
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:BSAGblksres
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:BSPamfrms
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:BINDicator
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:PMODe
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:MRETrans
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:IPReduction
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:CBARring
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:PMIDentity
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:CDEScription
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:ECSending
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:LUPDate
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:DTX
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:IDENtity
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:MCC
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:LAC
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:RAC
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:BCC
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:IMEirequest
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:CREQuest
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:PRAupdate
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:PLUPdate

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CELL:PSDomain
	CONFigure:GSM:SIGNaling<Instance>:CELL:NSUPport
	CONFigure:GSM:SIGNaling<Instance>:CELL:ECIot
	CONFigure:GSM:SIGNaling<Instance>:CELL:DTMode
	CONFigure:GSM:SIGNaling<Instance>:CELL:BSAGblksres
	CONFigure:GSM:SIGNaling<Instance>:CELL:BSPamfrms
	CONFigure:GSM:SIGNaling<Instance>:CELL:BINDicator
	CONFigure:GSM:SIGNaling<Instance>:CELL:PMODe
	CONFigure:GSM:SIGNaling<Instance>:CELL:MRETrans
	CONFigure:GSM:SIGNaling<Instance>:CELL:IPReduction
	CONFigure:GSM:SIGNaling<Instance>:CELL:CBARring
	CONFigure:GSM:SIGNaling<Instance>:CELL:PMIDentity
	CONFigure:GSM:SIGNaling<Instance>:CELL:CDEScription
	CONFigure:GSM:SIGNaling<Instance>:CELL:ECSending
	CONFigure:GSM:SIGNaling<Instance>:CELL:LUPDate
	CONFigure:GSM:SIGNaling<Instance>:CELL:DTX
	CONFigure:GSM:SIGNaling<Instance>:CELL:IDENtity
	CONFigure:GSM:SIGNaling<Instance>:CELL:MCC
	CONFigure:GSM:SIGNaling<Instance>:CELL:LAC
	CONFigure:GSM:SIGNaling<Instance>:CELL:RAC
	CONFigure:GSM:SIGNaling<Instance>:CELL:BCC
	CONFigure:GSM:SIGNaling<Instance>:CELL:IMEirequest
	CONFigure:GSM:SIGNaling<Instance>:CELL:CREQuest
	CONFigure:GSM:SIGNaling<Instance>:CELL:PRAupdate
	CONFigure:GSM:SIGNaling<Instance>:CELL:PLUPdate



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cell.Cell
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_ReSelection.rst
	Configure_Cell_Imsi.rst
	Configure_Cell_Ncc.rst
	Configure_Cell_Cswitched.rst
	Configure_Cell_Pswitched.rst
	Configure_Cell_Security.rst
	Configure_Cell_Rcause.rst
	Configure_Cell_Mnc.rst
	Configure_Cell_Rtms.rst
	Configure_Cell_Rtbs.rst
	Configure_Cell_Atimeout.rst
	Configure_Cell_Time.rst
	Configure_Cell_Sync.rst