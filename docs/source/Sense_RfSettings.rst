RfSettings
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:RFSettings:EPOWer
	single: SENSe:GSM:SIGNaling<Instance>:RFSettings:EFRequency

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:RFSettings:EPOWer
	SENSe:GSM:SIGNaling<Instance>:RFSettings:EFRequency



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.RfSettings.RfSettings
	:members:
	:undoc-members:
	:noindex: