Ethroughput
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:CONNection:ETHRoughput:UL
	single: SENSe:GSM:SIGNaling<Instance>:CONNection:ETHRoughput:DL

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:CONNection:ETHRoughput:UL
	SENSe:GSM:SIGNaling<Instance>:CONNection:ETHRoughput:DL



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Connection_.Ethroughput.Ethroughput
	:members:
	:undoc-members:
	:noindex: