ScFading
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Route_.Scenario_.ScFading.ScFading
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.scFading.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_ScFading_Flexible.rst