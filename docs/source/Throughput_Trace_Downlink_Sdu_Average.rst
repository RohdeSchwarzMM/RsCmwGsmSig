Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:AVERage
	single: READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:AVERage

.. code-block:: python

	FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:AVERage
	READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:AVERage



.. autoclass:: RsCmwGsmSig.Implementations.Throughput_.Trace_.Downlink_.Sdu_.Average.Average
	:members:
	:undoc-members:
	:noindex: