Hsn
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.Hopping_.Hsn.Hsn
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.hopping.hsn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Hopping_Hsn_Tch.rst