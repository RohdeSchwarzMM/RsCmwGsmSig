Eattenuation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:EATTenuation:INPut

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:RFSettings:EATTenuation:INPut



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.Eattenuation.Eattenuation
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.eattenuation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Eattenuation_Output.rst
	Configure_RfSettings_Eattenuation_Bcch.rst