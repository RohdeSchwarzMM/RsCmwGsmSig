Flexible
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:GSM:SIGNaling<Instance>:SCENario:SCFading:FLEXible:EXTernal
	single: ROUTe:GSM:SIGNaling<Instance>:SCENario:SCFading:FLEXible:INTernal

.. code-block:: python

	ROUTe:GSM:SIGNaling<Instance>:SCENario:SCFading:FLEXible:EXTernal
	ROUTe:GSM:SIGNaling<Instance>:SCENario:SCFading:FLEXible:INTernal



.. autoclass:: RsCmwGsmSig.Implementations.Route_.Scenario_.ScFading_.Flexible.Flexible
	:members:
	:undoc-members:
	:noindex: