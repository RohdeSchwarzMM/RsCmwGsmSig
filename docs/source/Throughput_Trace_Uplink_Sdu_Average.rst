Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:AVERage
	single: READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:AVERage

.. code-block:: python

	FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:AVERage
	READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:AVERage



.. autoclass:: RsCmwGsmSig.Implementations.Throughput_.Trace_.Uplink_.Sdu_.Average.Average
	:members:
	:undoc-members:
	:noindex: