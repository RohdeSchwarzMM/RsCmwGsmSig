Gamma
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:PSWitched:GAMMa:UL

.. code-block:: python

	PREPare:GSM:SIGNaling<Instance>:HANDover:PSWitched:GAMMa:UL



.. autoclass:: RsCmwGsmSig.Implementations.Prepare_.Handover_.Pswitched_.Gamma.Gamma
	:members:
	:undoc-members:
	:noindex: