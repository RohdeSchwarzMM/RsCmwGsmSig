Epsk
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:WB:FRATe:EPSK:DL
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:WB:FRATe:EPSK:UL

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:WB:FRATe:EPSK:DL
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:WB:FRATe:EPSK:UL



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.MssInfo_.Amr_.Cmode_.Wb_.Frate_.Epsk.Epsk
	:members:
	:undoc-members:
	:noindex: