Downlink
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Pswitched_.Sconfig_.Enable_.Downlink.Downlink
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pswitched.sconfig.enable.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pswitched_Sconfig_Enable_Downlink_Carrier.rst