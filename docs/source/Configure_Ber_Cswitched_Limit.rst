Limit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:LIMit:BER
	single: CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:LIMit:CIIBits
	single: CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:LIMit:CIBBits
	single: CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:LIMit:FER
	single: CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:LIMit:FFACch
	single: CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:LIMit:FSACch

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:LIMit:BER
	CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:LIMit:CIIBits
	CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:LIMit:CIBBits
	CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:LIMit:FER
	CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:LIMit:FFACch
	CONFigure:GSM:SIGNaling<Instance>:BER:CSWitched:LIMit:FSACch



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Ber_.Cswitched_.Limit.Limit
	:members:
	:undoc-members:
	:noindex: