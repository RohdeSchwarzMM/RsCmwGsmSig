Thresholds
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:NCELl:TDSCdma:THResholds:HIGH

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:NCELl:TDSCdma:THResholds:HIGH



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Ncell_.Tdscdma_.Thresholds.Thresholds
	:members:
	:undoc-members:
	:noindex: