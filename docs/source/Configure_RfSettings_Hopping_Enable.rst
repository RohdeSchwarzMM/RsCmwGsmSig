Enable
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.Hopping_.Enable.Enable
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.hopping.enable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Hopping_Enable_Tch.rst