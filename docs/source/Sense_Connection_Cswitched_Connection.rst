Connection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:CONNection:CSWitched:CONNection:ATTempt
	single: SENSe:GSM:SIGNaling<Instance>:CONNection:CSWitched:CONNection:REJect

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:CONNection:CSWitched:CONNection:ATTempt
	SENSe:GSM:SIGNaling<Instance>:CONNection:CSWitched:CONNection:REJect



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Connection_.Cswitched_.Connection.Connection
	:members:
	:undoc-members:
	:noindex: