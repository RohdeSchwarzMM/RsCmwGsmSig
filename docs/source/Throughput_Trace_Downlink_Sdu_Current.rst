Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:CURRent
	single: READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:CURRent

.. code-block:: python

	FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:CURRent
	READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:SDU:CURRent



.. autoclass:: RsCmwGsmSig.Implementations.Throughput_.Trace_.Downlink_.Sdu_.Current.Current
	:members:
	:undoc-members:
	:noindex: