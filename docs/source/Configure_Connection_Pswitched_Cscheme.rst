Cscheme
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:CSCHeme:UL

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:PSWitched:CSCHeme:UL



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Pswitched_.Cscheme.Cscheme
	:members:
	:undoc-members:
	:noindex: