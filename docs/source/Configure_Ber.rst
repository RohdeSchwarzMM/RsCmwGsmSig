Ber
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Ber.Ber
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ber.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ber_Cswitched.rst
	Configure_Ber_Pswitched.rst