Cell<CellNo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.configure.ncell.lte.cell.repcap_cellNo_get()
	driver.configure.ncell.lte.cell.repcap_cellNo_set(repcap.CellNo.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:NCELl:LTE:CELL<CellNo>

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:NCELl:LTE:CELL<CellNo>



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Ncell_.Lte_.Cell.Cell
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ncell.lte.cell.clone()