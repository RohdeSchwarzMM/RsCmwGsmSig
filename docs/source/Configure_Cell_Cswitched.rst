Cswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:CSWitched:CREQuest
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:CSWitched:IARTimer

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CELL:CSWitched:CREQuest
	CONFigure:GSM:SIGNaling<Instance>:CELL:CSWitched:IARTimer



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cell_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex: