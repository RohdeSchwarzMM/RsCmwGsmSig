Pmax
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:PMAX:BCCH

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:RFSettings:PMAX:BCCH



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.Pmax.Pmax
	:members:
	:undoc-members:
	:noindex: