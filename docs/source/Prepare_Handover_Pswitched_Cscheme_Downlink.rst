Downlink
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Prepare_.Handover_.Pswitched_.Cscheme_.Downlink.Downlink
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prepare.handover.pswitched.cscheme.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prepare_Handover_Pswitched_Cscheme_Downlink_Carrier.rst