Gmsk
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:NB:HRATe:GMSK:DL
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:NB:HRATe:GMSK:UL

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:NB:HRATe:GMSK:DL
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:NB:HRATe:GMSK:UL



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.MssInfo_.Amr_.Cmode_.Nb_.Hrate_.Gmsk.Gmsk
	:members:
	:undoc-members:
	:noindex: