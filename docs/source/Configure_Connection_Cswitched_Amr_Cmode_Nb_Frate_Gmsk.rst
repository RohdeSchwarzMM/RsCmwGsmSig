Gmsk
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:CMODe:NB:FRATe:GMSK:DL
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:CMODe:NB:FRATe:GMSK:UL

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:CMODe:NB:FRATe:GMSK:DL
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:CMODe:NB:FRATe:GMSK:UL



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Cswitched_.Amr_.Cmode_.Nb_.Frate_.Gmsk.Gmsk
	:members:
	:undoc-members:
	:noindex: