State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:SIGNaling<Instance>:CPERformance:STATe

.. code-block:: python

	FETCh:GSM:SIGNaling<Instance>:CPERformance:STATe



.. autoclass:: RsCmwGsmSig.Implementations.Cperformance_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.cperformance.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Cperformance_State_All.rst