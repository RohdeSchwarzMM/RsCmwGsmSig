Throughput
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STOP:GSM:SIGNaling<Instance>:THRoughput
	single: ABORt:GSM:SIGNaling<Instance>:THRoughput
	single: INITiate:GSM:SIGNaling<Instance>:THRoughput
	single: FETCh:GSM:SIGNaling<Instance>:THRoughput
	single: READ:GSM:SIGNaling<Instance>:THRoughput

.. code-block:: python

	STOP:GSM:SIGNaling<Instance>:THRoughput
	ABORt:GSM:SIGNaling<Instance>:THRoughput
	INITiate:GSM:SIGNaling<Instance>:THRoughput
	FETCh:GSM:SIGNaling<Instance>:THRoughput
	READ:GSM:SIGNaling<Instance>:THRoughput



.. autoclass:: RsCmwGsmSig.Implementations.Throughput.Throughput
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.throughput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Throughput_State.rst
	Throughput_Trace.rst