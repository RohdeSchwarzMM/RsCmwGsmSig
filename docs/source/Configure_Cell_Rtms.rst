Rtms
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:RTMS:CSWitched

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CELL:RTMS:CSWitched



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cell_.Rtms.Rtms
	:members:
	:undoc-members:
	:noindex: