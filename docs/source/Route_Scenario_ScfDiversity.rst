ScfDiversity
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Route_.Scenario_.ScfDiversity.ScfDiversity
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.scfDiversity.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_ScfDiversity_Flexible.rst