Tch
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.Channel_.Tch.Tch
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.channel.tch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Channel_Tch_Carrier.rst