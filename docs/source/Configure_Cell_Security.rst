Security
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:SECurity:AUTHenticat
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:SECurity:SKEY
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:SECurity:SIMCard

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CELL:SECurity:AUTHenticat
	CONFigure:GSM:SIGNaling<Instance>:CELL:SECurity:SKEY
	CONFigure:GSM:SIGNaling<Instance>:CELL:SECurity:SIMCard



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cell_.Security.Security
	:members:
	:undoc-members:
	:noindex: