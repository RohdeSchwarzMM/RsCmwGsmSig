Mslot
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:MSLot:UL

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:MSLot:UL



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Mslot.Mslot
	:members:
	:undoc-members:
	:noindex: