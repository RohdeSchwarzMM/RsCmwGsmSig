Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:CURRent
	single: READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:CURRent

.. code-block:: python

	FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:CURRent
	READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:CURRent



.. autoclass:: RsCmwGsmSig.Implementations.Throughput_.Trace_.Downlink_.Pdu_.Current.Current
	:members:
	:undoc-members:
	:noindex: