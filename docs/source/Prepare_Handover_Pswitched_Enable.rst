Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:PSWitched:ENABle:UL

.. code-block:: python

	PREPare:GSM:SIGNaling<Instance>:HANDover:PSWitched:ENABle:UL



.. autoclass:: RsCmwGsmSig.Implementations.Prepare_.Handover_.Pswitched_.Enable.Enable
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prepare.handover.pswitched.enable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prepare_Handover_Pswitched_Enable_Downlink.rst