Time
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:TIME:TSOurce
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:TIME:DATE
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:TIME:TIME
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:TIME:DSTime
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:TIME:LTZoffset
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:TIME:SATTach
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:TIME:SNName

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CELL:TIME:TSOurce
	CONFigure:GSM:SIGNaling<Instance>:CELL:TIME:DATE
	CONFigure:GSM:SIGNaling<Instance>:CELL:TIME:TIME
	CONFigure:GSM:SIGNaling<Instance>:CELL:TIME:DSTime
	CONFigure:GSM:SIGNaling<Instance>:CELL:TIME:LTZoffset
	CONFigure:GSM:SIGNaling<Instance>:CELL:TIME:SATTach
	CONFigure:GSM:SIGNaling<Instance>:CELL:TIME:SNName



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cell_.Time.Time
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.time.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Time_Snow.rst