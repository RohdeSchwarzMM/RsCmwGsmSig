RfSettings
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:MLOFfset
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:ENPower
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:ENPMode
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:UMARgin

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:RFSettings:MLOFfset
	CONFigure:GSM:SIGNaling<Instance>:RFSettings:ENPower
	CONFigure:GSM:SIGNaling<Instance>:RFSettings:ENPMode
	CONFigure:GSM:SIGNaling<Instance>:RFSettings:UMARgin



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings.RfSettings
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Eattenuation.rst
	Configure_RfSettings_Channel.rst
	Configure_RfSettings_Level.rst
	Configure_RfSettings_Pmax.rst
	Configure_RfSettings_Foffset.rst
	Configure_RfSettings_Pcl.rst
	Configure_RfSettings_ChcCombined.rst
	Configure_RfSettings_Edc.rst
	Configure_RfSettings_Hopping.rst