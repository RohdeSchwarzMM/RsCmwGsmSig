Catalog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:CATalog:DESTination

.. code-block:: python

	PREPare:GSM:SIGNaling<Instance>:HANDover:CATalog:DESTination



.. autoclass:: RsCmwGsmSig.Implementations.Prepare_.Handover_.Catalog.Catalog
	:members:
	:undoc-members:
	:noindex: