Connection
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Clean_.Connection_.Cswitched_.Connection.Connection
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.clean.connection.cswitched.connection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Clean_Connection_Cswitched_Connection_Attempt.rst
	Clean_Connection_Cswitched_Connection_Reject.rst