State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:SIGNaling<Instance>:PSWitched:STATe

.. code-block:: python

	FETCh:GSM:SIGNaling<Instance>:PSWitched:STATe



.. autoclass:: RsCmwGsmSig.Implementations.Pswitched_.State.State
	:members:
	:undoc-members:
	:noindex: