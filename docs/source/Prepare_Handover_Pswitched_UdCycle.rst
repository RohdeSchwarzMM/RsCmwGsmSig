UdCycle
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:PSWitched:UDCYcle:DL

.. code-block:: python

	PREPare:GSM:SIGNaling<Instance>:HANDover:PSWitched:UDCYcle:DL



.. autoclass:: RsCmwGsmSig.Implementations.Prepare_.Handover_.Pswitched_.UdCycle.UdCycle
	:members:
	:undoc-members:
	:noindex: