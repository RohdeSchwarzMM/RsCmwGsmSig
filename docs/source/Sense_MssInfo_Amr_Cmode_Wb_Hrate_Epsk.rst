Epsk
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:WB:HRATe:EPSK:DL
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:WB:HRATe:EPSK:UL

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:WB:HRATe:EPSK:DL
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:WB:HRATe:EPSK:UL



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.MssInfo_.Amr_.Cmode_.Wb_.Hrate_.Epsk.Epsk
	:members:
	:undoc-members:
	:noindex: