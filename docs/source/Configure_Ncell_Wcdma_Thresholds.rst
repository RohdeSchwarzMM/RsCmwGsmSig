Thresholds
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:NCELl:WCDMa:THResholds:HIGH

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:NCELl:WCDMa:THResholds:HIGH



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Ncell_.Wcdma_.Thresholds.Thresholds
	:members:
	:undoc-members:
	:noindex: