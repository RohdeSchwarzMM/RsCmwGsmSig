Pswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:GSM:SIGNaling<Instance>:PSWitched:ACTion

.. code-block:: python

	CALL:GSM:SIGNaling<Instance>:PSWitched:ACTion



.. autoclass:: RsCmwGsmSig.Implementations.Call_.Pswitched.Pswitched
	:members:
	:undoc-members:
	:noindex: