MssInfo
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:RXPower
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:APN
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:IMSI
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:IMEI
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:DNUMber
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:TTY
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:SCATegory
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:BANDs
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:EDALlocation

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:MSSinfo:RXPower
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:APN
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:IMSI
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:IMEI
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:DNUMber
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:TTY
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:SCATegory
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:BANDs
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:EDALlocation



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.MssInfo.MssInfo
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.mssInfo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_MssInfo_Amr.rst
	Sense_MssInfo_MsAddress.rst
	Sense_MssInfo_MsClass.rst
	Sense_MssInfo_Codec.rst
	Sense_MssInfo_Vamos.rst
	Sense_MssInfo_Tcapability.rst