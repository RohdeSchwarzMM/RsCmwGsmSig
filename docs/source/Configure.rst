Configure
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:ETOE

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:ETOE



.. autoclass:: RsCmwGsmSig.Implementations.Configure.Configure
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Band.rst
	Configure_DualBand.rst
	Configure_Mslot.rst
	Configure_RfSettings.rst
	Configure_IqIn.rst
	Configure_Fading.rst
	Configure_Connection.rst
	Configure_Ncell.rst
	Configure_Cell.rst
	Configure_Trigger.rst
	Configure_Rreport.rst
	Configure_Sms.rst
	Configure_Cbs.rst
	Configure_Ber.rst
	Configure_Bler.rst
	Configure_Throughput.rst
	Configure_Cperformance.rst
	Configure_Mmonitor.rst
	Configure_MsReport.rst