Vamos
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:VAMos:ENABle
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:VAMos:MSLevel
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:VAMos

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:VAMos:ENABle
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:VAMos:MSLevel
	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:VAMos



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Cswitched_.Vamos.Vamos
	:members:
	:undoc-members:
	:noindex: