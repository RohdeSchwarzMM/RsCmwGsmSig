Mbep
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:INTermediate:GSM:SIGNaling<Instance>:BER:CSWitched:MBEP

.. code-block:: python

	FETCh:INTermediate:GSM:SIGNaling<Instance>:BER:CSWitched:MBEP



.. autoclass:: RsCmwGsmSig.Implementations.Intermediate_.Ber_.Cswitched_.Mbep.Mbep
	:members:
	:undoc-members:
	:noindex: