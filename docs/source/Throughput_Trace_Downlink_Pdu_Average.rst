Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:AVERage
	single: READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:AVERage

.. code-block:: python

	FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:AVERage
	READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:DL:PDU:AVERage



.. autoclass:: RsCmwGsmSig.Implementations.Throughput_.Trace_.Downlink_.Pdu_.Average.Average
	:members:
	:undoc-members:
	:noindex: