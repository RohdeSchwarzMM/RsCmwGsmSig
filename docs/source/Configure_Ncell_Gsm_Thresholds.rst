Thresholds
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:NCELl:GSM:THResholds:HIGH

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:NCELl:GSM:THResholds:HIGH



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Ncell_.Gsm_.Thresholds.Thresholds
	:members:
	:undoc-members:
	:noindex: