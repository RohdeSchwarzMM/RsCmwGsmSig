Gmsk
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:WB:FRATe:GMSK:DL
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:WB:FRATe:GMSK:UL

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:WB:FRATe:GMSK:DL
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:AMR:CMODe:WB:FRATe:GMSK:UL



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.MssInfo_.Amr_.Cmode_.Wb_.Frate_.Gmsk.Gmsk
	:members:
	:undoc-members:
	:noindex: