Path<Path>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.sense.iqOut.path.repcap_path_get()
	driver.sense.iqOut.path.repcap_path_set(repcap.Path.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:IQOut:PATH<Path>

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:IQOut:PATH<Path>



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.IqOut_.Path.Path
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.iqOut.path.clone()