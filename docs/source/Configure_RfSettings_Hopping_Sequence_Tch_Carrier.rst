Carrier<Carrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.rfSettings.hopping.sequence.tch.carrier.repcap_carrier_get()
	driver.configure.rfSettings.hopping.sequence.tch.carrier.repcap_carrier_set(repcap.Carrier.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:RFSettings:HOPPing:SEQuence:TCH:CARRier<Carrier>

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:RFSettings:HOPPing:SEQuence:TCH:CARRier<Carrier>



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.RfSettings_.Hopping_.Sequence_.Tch_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.hopping.sequence.tch.carrier.clone()