Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:CURRent
	single: READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:CURRent

.. code-block:: python

	FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:CURRent
	READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:SDU:CURRent



.. autoclass:: RsCmwGsmSig.Implementations.Throughput_.Trace_.Uplink_.Sdu_.Current.Current
	:members:
	:undoc-members:
	:noindex: