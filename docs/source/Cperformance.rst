Cperformance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STOP:GSM:SIGNaling<Instance>:CPERformance
	single: ABORt:GSM:SIGNaling<Instance>:CPERformance
	single: INITiate:GSM:SIGNaling<Instance>:CPERformance
	single: READ:GSM:SIGNaling<Instance>:CPERformance
	single: FETCh:GSM:SIGNaling<Instance>:CPERformance

.. code-block:: python

	STOP:GSM:SIGNaling<Instance>:CPERformance
	ABORt:GSM:SIGNaling<Instance>:CPERformance
	INITiate:GSM:SIGNaling<Instance>:CPERformance
	READ:GSM:SIGNaling<Instance>:CPERformance
	FETCh:GSM:SIGNaling<Instance>:CPERformance



.. autoclass:: RsCmwGsmSig.Implementations.Cperformance.Cperformance
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.cperformance.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Cperformance_State.rst