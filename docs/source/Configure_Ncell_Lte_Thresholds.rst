Thresholds
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:NCELl:LTE:THResholds:HIGH

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:NCELl:LTE:THResholds:HIGH



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Ncell_.Lte_.Thresholds.Thresholds
	:members:
	:undoc-members:
	:noindex: