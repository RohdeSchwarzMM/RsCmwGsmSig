RepCaps
=========





Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Range:
	Inst1 .. Inst61
	# All values (61x):
	Inst1 | Inst2 | Inst3 | Inst4 | Inst5 | Inst6 | Inst7 | Inst8
	Inst9 | Inst10 | Inst11 | Inst12 | Inst13 | Inst14 | Inst15 | Inst16
	Inst17 | Inst18 | Inst19 | Inst20 | Inst21 | Inst22 | Inst23 | Inst24
	Inst25 | Inst26 | Inst27 | Inst28 | Inst29 | Inst30 | Inst31 | Inst32
	Inst33 | Inst34 | Inst35 | Inst36 | Inst37 | Inst38 | Inst39 | Inst40
	Inst41 | Inst42 | Inst43 | Inst44 | Inst45 | Inst46 | Inst47 | Inst48
	Inst49 | Inst50 | Inst51 | Inst52 | Inst53 | Inst54 | Inst55 | Inst56
	Inst57 | Inst58 | Inst59 | Inst60 | Inst61

Carrier
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Carrier.Nr1
	# Values (2x):
	Nr1 | Nr2

CellNo
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.CellNo.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

GsmCellNo
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.GsmCellNo.Nr1
	# Range:
	Nr1 .. Nr16
	# All values (16x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16

HsrQAM
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.HsrQAM.QAM16
	# Values (2x):
	QAM16 | QAM32

IPversion
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.IPversion.IPv4
	# Values (2x):
	IPv4 | IPv6

NsrQAM
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.NsrQAM.QAM16
	# Values (2x):
	QAM16 | QAM32

Output
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Output.Nr1
	# Values (2x):
	Nr1 | Nr2

Path
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Path.Nr1
	# Values (2x):
	Nr1 | Nr2

