Pswitched
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Prepare_.Handover_.Pswitched.Pswitched
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prepare.handover.pswitched.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prepare_Handover_Pswitched_Enable.rst
	Prepare_Handover_Pswitched_Gamma.rst
	Prepare_Handover_Pswitched_Level.rst
	Prepare_Handover_Pswitched_Cscheme.rst
	Prepare_Handover_Pswitched_UdCycle.rst