Limit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:BER:PSWitched:LIMit:CIIBits
	single: CONFigure:GSM:SIGNaling<Instance>:BER:PSWitched:LIMit:DBLer
	single: CONFigure:GSM:SIGNaling<Instance>:BER:PSWitched:LIMit:USFBler

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:BER:PSWitched:LIMit:CIIBits
	CONFigure:GSM:SIGNaling<Instance>:BER:PSWitched:LIMit:DBLer
	CONFigure:GSM:SIGNaling<Instance>:BER:PSWitched:LIMit:USFBler



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Ber_.Pswitched_.Limit.Limit
	:members:
	:undoc-members:
	:noindex: