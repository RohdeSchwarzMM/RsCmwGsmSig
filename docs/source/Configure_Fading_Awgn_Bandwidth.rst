Bandwidth
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:FADing:AWGN:BWIDth:RATio
	single: CONFigure:GSM:SIGNaling<Instance>:FADing:AWGN:BWIDth:NOISe

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:FADing:AWGN:BWIDth:RATio
	CONFigure:GSM:SIGNaling<Instance>:FADing:AWGN:BWIDth:NOISe



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Fading_.Awgn_.Bandwidth.Bandwidth
	:members:
	:undoc-members:
	:noindex: