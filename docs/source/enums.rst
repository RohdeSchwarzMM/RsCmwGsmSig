Enums
=========

AcceptAfter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AcceptAfter.AA1
	# All values (8x):
	AA1 | AA2 | AA3 | AA4 | AA5 | AA6 | AA7 | IALL

AutoManualMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoManualMode.AUTO
	# All values (2x):
	AUTO | MANual

AutoMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoMode.AUTO
	# All values (3x):
	AUTO | OFF | ON

BandClass
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BandClass.AWS
	# Last value:
	value = enums.BandClass.USPC
	# All values (21x):
	AWS | B18M | IEXT | IM2K | JTAC | KCEL | KPCS | LO7C
	N45T | NA7C | NA8S | NA9C | NAPC | PA4M | PA8M | PS7C
	TACS | U25B | U25F | USC | USPC

BandIndicator
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BandIndicator.G18
	# All values (2x):
	G18 | G19

BbBoard
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BbBoard.BBR1
	# Last value:
	value = enums.BbBoard.SUW44
	# All values (140x):
	BBR1 | BBR11 | BBR12 | BBR13 | BBR14 | BBR2 | BBR21 | BBR22
	BBR23 | BBR24 | BBR3 | BBR31 | BBR32 | BBR33 | BBR34 | BBR4
	BBR41 | BBR42 | BBR43 | BBR44 | BBT1 | BBT11 | BBT12 | BBT13
	BBT14 | BBT2 | BBT21 | BBT22 | BBT23 | BBT24 | BBT3 | BBT31
	BBT32 | BBT33 | BBT34 | BBT4 | BBT41 | BBT42 | BBT43 | BBT44
	SUA012 | SUA034 | SUA056 | SUA078 | SUA1 | SUA11 | SUA112 | SUA12
	SUA13 | SUA134 | SUA14 | SUA15 | SUA156 | SUA16 | SUA17 | SUA178
	SUA18 | SUA2 | SUA21 | SUA212 | SUA22 | SUA23 | SUA234 | SUA24
	SUA25 | SUA256 | SUA26 | SUA27 | SUA278 | SUA28 | SUA3 | SUA31
	SUA312 | SUA32 | SUA33 | SUA334 | SUA34 | SUA35 | SUA356 | SUA36
	SUA37 | SUA378 | SUA38 | SUA4 | SUA41 | SUA412 | SUA42 | SUA43
	SUA434 | SUA44 | SUA45 | SUA456 | SUA46 | SUA47 | SUA478 | SUA48
	SUA5 | SUA6 | SUA7 | SUA8 | SUU1 | SUU11 | SUU12 | SUU13
	SUU14 | SUU2 | SUU21 | SUU22 | SUU23 | SUU24 | SUU3 | SUU31
	SUU32 | SUU33 | SUU34 | SUU4 | SUU41 | SUU42 | SUU43 | SUU44
	SUW1 | SUW11 | SUW12 | SUW13 | SUW14 | SUW2 | SUW21 | SUW22
	SUW23 | SUW24 | SUW3 | SUW31 | SUW32 | SUW33 | SUW34 | SUW4
	SUW41 | SUW42 | SUW43 | SUW44

BerCsMeasMode
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BerCsMeasMode.AIFer
	# Last value:
	value = enums.BerCsMeasMode.SQUality
	# All values (10x):
	AIFer | BBBurst | BER | BFI | FFACch | FSACch | MBEP | RFER
	RUFR | SQUality

BerPsMeasMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BerPsMeasMode.BDBLer
	# All values (3x):
	BDBLer | MBEP | UBONly

CallRelease
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CallRelease.IRELease
	# All values (3x):
	IRELease | LERelease | NRELease

CmSerRejectType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CmSerRejectType.ECALl
	# All values (7x):
	ECALl | ECSMs | NCALl | NCECall | NCSMs | NESMs | SMS

CodingGroup
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingGroup.DCMClass
	# All values (2x):
	DCMClass | GDCoding

CodingSchemeDownlink
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CodingSchemeDownlink.C1
	# Last value:
	value = enums.CodingSchemeDownlink.MC9
	# All values (29x):
	C1 | C2 | C3 | C4 | DA10 | DA11 | DA12 | DA5
	DA6 | DA7 | DA8 | DA9 | DB10 | DB11 | DB12 | DB5
	DB6 | DB7 | DB8 | DB9 | MC1 | MC2 | MC3 | MC4
	MC5 | MC6 | MC7 | MC8 | MC9

CodingSchemeUplink
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CodingSchemeUplink.C1
	# Last value:
	value = enums.CodingSchemeUplink.UB9
	# All values (26x):
	C1 | C2 | C3 | C4 | MC1 | MC2 | MC3 | MC4
	MC5 | MC6 | MC7 | MC8 | MC9 | UA10 | UA11 | UA7
	UA8 | UA9 | UB10 | UB11 | UB12 | UB5 | UB6 | UB7
	UB8 | UB9

ConnectError
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConnectError.ATIMeout
	# All values (7x):
	ATIMeout | IGNored | NERRor | PTIMeout | REJected | RLTimeout | STIMeout

ConnectRequest
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConnectRequest.ACCept
	# All values (3x):
	ACCept | IGNore | REJect

ControlAckBurst
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ControlAckBurst.ABURsts
	# All values (2x):
	ABURsts | NBURsts

CswAction
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CswAction.CONNect
	# All values (6x):
	CONNect | DISConnect | HANDover | OFF | ON | SMS

CswLoop
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CswLoop.A
	# All values (7x):
	A | B | C | D | I | OFF | ON

CswState
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CswState.ALER
	# Last value:
	value = enums.CswState.SYNC
	# All values (13x):
	ALER | CEST | CONN | IHANdover | IMS | LUPD | OFF | OHANdover
	ON | REL | RMESsage | SMESsage | SYNC

DigitsCount
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DigitsCount.THRee
	# All values (2x):
	THRee | TWO

DownlinkCodingScheme
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DownlinkCodingScheme.C1
	# Last value:
	value = enums.DownlinkCodingScheme.ON
	# All values (31x):
	C1 | C2 | C3 | C4 | DA10 | DA11 | DA12 | DA5
	DA6 | DA7 | DA8 | DA9 | DB10 | DB11 | DB12 | DB5
	DB6 | DB7 | DB8 | DB9 | MC1 | MC2 | MC3 | MC4
	MC5 | MC6 | MC7 | MC8 | MC9 | OFF | ON

DsTime
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DsTime.OFF
	# All values (4x):
	OFF | ON | P1H | P2H

EightPskPowerClass
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EightPskPowerClass.E1
	# All values (4x):
	E1 | E2 | E3 | U

FadingBoard
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.FadingBoard.FAD012
	# Last value:
	value = enums.FadingBoard.FAD8
	# All values (60x):
	FAD012 | FAD034 | FAD056 | FAD078 | FAD1 | FAD11 | FAD112 | FAD12
	FAD13 | FAD134 | FAD14 | FAD15 | FAD156 | FAD16 | FAD17 | FAD178
	FAD18 | FAD2 | FAD21 | FAD212 | FAD22 | FAD23 | FAD234 | FAD24
	FAD25 | FAD256 | FAD26 | FAD27 | FAD278 | FAD28 | FAD3 | FAD31
	FAD312 | FAD32 | FAD33 | FAD334 | FAD34 | FAD35 | FAD356 | FAD36
	FAD37 | FAD378 | FAD38 | FAD4 | FAD41 | FAD412 | FAD42 | FAD43
	FAD434 | FAD44 | FAD45 | FAD456 | FAD46 | FAD47 | FAD478 | FAD48
	FAD5 | FAD6 | FAD7 | FAD8

FadingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FadingMode.NORMal
	# All values (2x):
	NORMal | USER

FadingStandard
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.FadingStandard.E100
	# Last value:
	value = enums.FadingStandard.TU60
	# All values (30x):
	E100 | E50 | E60 | H100 | H120 | H200 | HT100 | HT120
	HT200 | R130 | R250 | R300 | R500 | T100 | T1P5 | T25
	T3 | T3P6 | T50 | T6 | T60 | TI5 | TU100 | TU1P5
	TU25 | TU3 | TU3P6 | TU50 | TU6 | TU60

FrameTriggerMod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrameTriggerMod.EVERy
	# All values (5x):
	EVERy | EWIDle | M104 | M26 | M52

GeographicScope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeographicScope.CIMMediate
	# All values (4x):
	CIMMediate | CNORmal | LOCation | PLMN

HandoverDestination
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HandoverDestination.CDMA
	# All values (6x):
	CDMA | EVDO | GSM | LTE | TDSCdma | WCDMa

HandoverMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HandoverMode.CCORder
	# All values (4x):
	CCORder | DUALband | HANDover | REDirection

HandoverState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HandoverState.DUALband
	# All values (2x):
	DUALband | OFF

InsertLossMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InsertLossMode.LACP
	# All values (3x):
	LACP | NORMal | USER

IpAddrIndex
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpAddrIndex.IP1
	# All values (3x):
	IP1 | IP2 | IP3

LastMessageSent
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LastMessageSent.FAILed
	# All values (4x):
	FAILed | OFF | ON | SUCCessful

LmQuantity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LmQuantity.RSRP
	# All values (2x):
	RSRP | RSRQ

LocationUpdate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LocationUpdate.ALWays
	# All values (2x):
	ALWays | AUTO

LogCategory
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LogCategory.CONTinue
	# All values (4x):
	CONTinue | ERRor | INFO | WARNing

MainState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MainState.OFF
	# All values (3x):
	OFF | ON | RFHandover

MessageClass
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MessageClass.CL0
	# All values (5x):
	CL0 | CL1 | CL2 | CL3 | NONE

MsgIdSeverity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MsgIdSeverity.AAMBer
	# All values (5x):
	AAMBer | AEXTreme | APResidentia | ASEVere | UDEFined

NbCodec
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NbCodec.C0475
	# Last value:
	value = enums.NbCodec.ON
	# All values (10x):
	C0475 | C0515 | C0590 | C0670 | C0740 | C0795 | C1020 | C1220
	OFF | ON

NetworkSupport
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetworkSupport.EGPRs
	# All values (2x):
	EGPRs | GPRS

NominalPowerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NominalPowerMode.AUToranging
	# All values (3x):
	AUToranging | MANual | ULPC

OperBandGsm
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OperBandGsm.G04
	# All values (6x):
	G04 | G085 | G09 | G18 | G19 | GT081

OperBandLte
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.OperBandLte.OB1
	# Last value:
	value = enums.OperBandLte.OB9
	# All values (67x):
	OB1 | OB10 | OB11 | OB12 | OB13 | OB14 | OB15 | OB16
	OB17 | OB18 | OB19 | OB2 | OB20 | OB21 | OB22 | OB23
	OB24 | OB25 | OB250 | OB252 | OB255 | OB26 | OB27 | OB28
	OB29 | OB3 | OB30 | OB31 | OB32 | OB33 | OB34 | OB35
	OB36 | OB37 | OB38 | OB39 | OB4 | OB40 | OB41 | OB42
	OB43 | OB44 | OB45 | OB46 | OB48 | OB49 | OB5 | OB50
	OB51 | OB52 | OB6 | OB65 | OB66 | OB67 | OB68 | OB69
	OB7 | OB70 | OB71 | OB72 | OB73 | OB74 | OB75 | OB76
	OB8 | OB85 | OB9

OperBandTdsCdma
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OperBandTdsCdma.OB1
	# All values (3x):
	OB1 | OB2 | OB3

OperBandWcdma
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.OperBandWcdma.OB1
	# Last value:
	value = enums.OperBandWcdma.OBS3
	# All values (24x):
	OB1 | OB10 | OB11 | OB12 | OB13 | OB14 | OB19 | OB2
	OB20 | OB21 | OB22 | OB25 | OB26 | OB3 | OB4 | OB5
	OB6 | OB7 | OB8 | OB9 | OBL1 | OBS1 | OBS2 | OBS3

PageMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PageMode.NPAGing
	# All values (2x):
	NPAGing | PREorganize

Paging
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Paging.IMSI
	# All values (2x):
	IMSI | TMSI

PcmChannel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PcmChannel.BCCH
	# All values (2x):
	BCCH | PDCH

PowerReductionField
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerReductionField.DB0
	# All values (4x):
	DB0 | DB3 | DB7 | NUSable

PowerReductionMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerReductionMode.PMA
	# All values (2x):
	PMA | PMB

Priority
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Priority.BACKground
	# All values (3x):
	BACKground | HIGH | NORMal

Profile
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Profile.OFF
	# All values (5x):
	OFF | ON | SUSer | TUDTx | TUSer

PswAction
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PswAction.CONNect
	# All values (7x):
	CONNect | DISConnect | HANDover | OFF | ON | RPContext | SMS

PswitchedService
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PswitchedService.BLER
	# All values (4x):
	BLER | SRB | TMA | TMB

PswPowerReduction
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PswPowerReduction.DB0
	# Last value:
	value = enums.PswPowerReduction.DB8
	# All values (16x):
	DB0 | DB10 | DB12 | DB14 | DB16 | DB18 | DB2 | DB20
	DB22 | DB24 | DB26 | DB28 | DB30 | DB4 | DB6 | DB8

PswState
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PswState.AIPR
	# Last value:
	value = enums.PswState.TBF
	# All values (12x):
	AIPR | ATT | CTIP | DIPR | OFF | ON | PAIP | PDIP
	PDP | RAUP | REL | TBF

ReactionMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReactionMode.ACCept
	# All values (2x):
	ACCept | REJect

RejectionCause1
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RejectionCause1.C100
	# Last value:
	value = enums.RejectionCause1.ON
	# All values (30x):
	C100 | C101 | C11 | C111 | C12 | C13 | C15 | C17
	C2 | C20 | C21 | C22 | C23 | C25 | C3 | C32
	C33 | C34 | C38 | C4 | C48 | C5 | C6 | C95
	C96 | C97 | C98 | C99 | OFF | ON

RejectionCause2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RejectionCause2.C10
	# Last value:
	value = enums.RejectionCause2.ON
	# All values (38x):
	C10 | C100 | C101 | C11 | C111 | C12 | C13 | C14
	C15 | C16 | C17 | C2 | C20 | C21 | C22 | C23
	C25 | C28 | C3 | C32 | C33 | C34 | C38 | C4
	C40 | C48 | C5 | C6 | C7 | C8 | C9 | C95
	C96 | C97 | C98 | C99 | OFF | ON

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

RestartMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RestartMode.AUTO
	# All values (3x):
	AUTO | MANual | TRIGger

RxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConnector.I11I
	# Last value:
	value = enums.RxConnector.RH8
	# All values (154x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IF1 | IF2 | IF3 | IQ1I | IQ3I | IQ5I | IQ7I | R11
	R11C | R12 | R12C | R12I | R13 | R13C | R14 | R14C
	R14I | R15 | R16 | R17 | R18 | R21 | R21C | R22
	R22C | R22I | R23 | R23C | R24 | R24C | R24I | R25
	R26 | R27 | R28 | R31 | R31C | R32 | R32C | R32I
	R33 | R33C | R34 | R34C | R34I | R35 | R36 | R37
	R38 | R41 | R41C | R42 | R42C | R42I | R43 | R43C
	R44 | R44C | R44I | R45 | R46 | R47 | R48 | RA1
	RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8 | RB1
	RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8 | RC1
	RC2 | RC3 | RC4 | RC5 | RC6 | RC7 | RC8 | RD1
	RD2 | RD3 | RD4 | RD5 | RD6 | RD7 | RD8 | RE1
	RE2 | RE3 | RE4 | RE5 | RE6 | RE7 | RE8 | RF1
	RF1C | RF2 | RF2C | RF2I | RF3 | RF3C | RF4 | RF4C
	RF4I | RF5 | RF5C | RF6 | RF6C | RF7 | RF8 | RFAC
	RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5 | RG6
	RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5 | RH6
	RH7 | RH8

RxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConverter.IRX1
	# Last value:
	value = enums.RxConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

RxPower
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RxPower.INV
	# All values (6x):
	INV | NAV | NCAP | OFL | OK | UFL

SampleRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SampleRate.M1
	# All values (8x):
	M1 | M100 | M15 | M19 | M3 | M30 | M7 | M9

Scenario
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Scenario.BATC
	# All values (6x):
	BATC | IORI | NAV | SCEL | SCF | SCFDiversity

SignalingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalingMode.LTRR
	# All values (2x):
	LTRR | RATScch

SimCardType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SimCardType.C2G
	# All values (2x):
	C2G | C3G

SmsDataCoding
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SmsDataCoding.BIT7
	# All values (2x):
	BIT7 | BIT8

SmsDomain
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SmsDomain.AUTO
	# All values (3x):
	AUTO | CS | PS

SourceInt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceInt.EXTernal
	# All values (2x):
	EXTernal | INTernal

SourceTime
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceTime.CMWTime
	# All values (2x):
	CMWTime | DATE

SpeechChannelCodingMode
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SpeechChannelCodingMode.ANFG
	# Last value:
	value = enums.SpeechChannelCodingMode.HV1
	# All values (9x):
	ANFG | ANH8 | ANHG | AWF8 | AWFG | AWH8 | FV1 | FV2
	HV1

SwitchedSourceMode
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SwitchedSourceMode.ALL0
	# Last value:
	value = enums.SwitchedSourceMode.UPATtern
	# All values (11x):
	ALL0 | ALL1 | ALTernating | ECHO | PR11 | PR15 | PR16 | PR9
	SP1 | SP2 | UPATtern

SyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SyncState.ADINtermed
	# All values (7x):
	ADINtermed | ADJusted | INValid | OFF | ON | PENDing | RFHandover

SyncZone
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SyncZone.NONE
	# All values (2x):
	NONE | Z1

TbfLevel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TbfLevel.EG2A
	# All values (4x):
	EG2A | EG2B | EGPRs | GPRS

TchAssignment
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TchAssignment.EARLy
	# All values (5x):
	EARLy | LATE | OFF | ON | VEARly

TxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConnector.I12O
	# Last value:
	value = enums.TxConnector.RH18
	# All values (77x):
	I12O | I14O | I16O | I18O | I22O | I24O | I26O | I28O
	I32O | I34O | I36O | I38O | I42O | I44O | I46O | I48O
	IF1 | IF2 | IF3 | IQ2O | IQ4O | IQ6O | IQ8O | R118
	R1183 | R1184 | R11C | R11O | R11O3 | R11O4 | R12C | R13C
	R13O | R14C | R214 | R218 | R21C | R21O | R22C | R23C
	R23O | R24C | R258 | R318 | R31C | R31O | R32C | R33C
	R33O | R34C | R418 | R41C | R41O | R42C | R43C | R43O
	R44C | RA18 | RB14 | RB18 | RC18 | RD18 | RE18 | RF18
	RF1C | RF1O | RF2C | RF3C | RF3O | RF4C | RF5C | RF6C
	RFAC | RFAO | RFBC | RG18 | RH18

TxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConverter.ITX1
	# Last value:
	value = enums.TxConverter.TX44
	# All values (40x):
	ITX1 | ITX11 | ITX12 | ITX13 | ITX14 | ITX2 | ITX21 | ITX22
	ITX23 | ITX24 | ITX3 | ITX31 | ITX32 | ITX33 | ITX34 | ITX4
	ITX41 | ITX42 | ITX43 | ITX44 | TX1 | TX11 | TX12 | TX13
	TX14 | TX2 | TX21 | TX22 | TX23 | TX24 | TX3 | TX31
	TX32 | TX33 | TX34 | TX4 | TX41 | TX42 | TX43 | TX44

UplinkCodingScheme
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.UplinkCodingScheme.C1
	# Last value:
	value = enums.UplinkCodingScheme.UB9
	# All values (28x):
	C1 | C2 | C3 | C4 | MC1 | MC2 | MC3 | MC4
	MC5 | MC6 | MC7 | MC8 | MC9 | OFF | ON | UA10
	UA11 | UA7 | UA8 | UA9 | UB10 | UB11 | UB12 | UB5
	UB6 | UB7 | UB8 | UB9

VamosMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VamosMode.AUTO
	# All values (3x):
	AUTO | VAM1 | VAM2

WbCodec
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.WbCodec.C0660
	# All values (7x):
	C0660 | C0885 | C1265 | C1585 | C2385 | OFF | ON

WmQuantity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.WmQuantity.ECNO
	# All values (2x):
	ECNO | RSCP

