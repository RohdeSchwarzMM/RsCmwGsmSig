Downlink
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Prepare_.Handover_.Pswitched_.Enable_.Downlink.Downlink
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prepare.handover.pswitched.enable.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prepare_Handover_Pswitched_Enable_Downlink_Carrier.rst