SctStamp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:TSOurce
	single: CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:DATE
	single: CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:TIME

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:TSOurce
	CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:DATE
	CONFigure:GSM:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:TIME



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Sms_.Outgoing_.SctStamp.SctStamp
	:members:
	:undoc-members:
	:noindex: