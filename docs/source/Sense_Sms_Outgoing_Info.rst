Info
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:SMS:OUTGoing:INFO:SEGMent
	single: SENSe:GSM:SIGNaling<Instance>:SMS:OUTGoing:INFO:LMSent

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:SMS:OUTGoing:INFO:SEGMent
	SENSe:GSM:SIGNaling<Instance>:SMS:OUTGoing:INFO:LMSent



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Sms_.Outgoing_.Info.Info
	:members:
	:undoc-members:
	:noindex: