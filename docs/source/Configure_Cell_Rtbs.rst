Rtbs
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:RTBS:CSWitched

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CELL:RTBS:CSWitched



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cell_.Rtbs.Rtbs
	:members:
	:undoc-members:
	:noindex: