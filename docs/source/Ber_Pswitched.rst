Pswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:GSM:SIGNaling<Instance>:BER:PSWitched
	single: STOP:GSM:SIGNaling<Instance>:BER:PSWitched
	single: ABORt:GSM:SIGNaling<Instance>:BER:PSWitched
	single: READ:GSM:SIGNaling<Instance>:BER:PSWitched
	single: FETCh:GSM:SIGNaling<Instance>:BER:PSWitched

.. code-block:: python

	INITiate:GSM:SIGNaling<Instance>:BER:PSWitched
	STOP:GSM:SIGNaling<Instance>:BER:PSWitched
	ABORt:GSM:SIGNaling<Instance>:BER:PSWitched
	READ:GSM:SIGNaling<Instance>:BER:PSWitched
	FETCh:GSM:SIGNaling<Instance>:BER:PSWitched



.. autoclass:: RsCmwGsmSig.Implementations.Ber_.Pswitched.Pswitched
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ber.pswitched.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ber_Pswitched_State.rst
	Ber_Pswitched_Carrier.rst