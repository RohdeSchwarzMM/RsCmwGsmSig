HsrQam<HsrQAM>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: QAM16 .. QAM32
	rc = driver.sense.rreport.hsrQam.repcap_hsrQAM_get()
	driver.sense.rreport.hsrQam.repcap_hsrQAM_set(repcap.HsrQAM.QAM16)





.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Rreport_.HsrQam.HsrQam
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.rreport.hsrQam.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Rreport_HsrQam_Mbep.rst
	Sense_Rreport_HsrQam_Cbep.rst