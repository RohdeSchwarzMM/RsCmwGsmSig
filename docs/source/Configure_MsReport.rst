MsReport
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:MSReport:WMQuantity
	single: CONFigure:GSM:SIGNaling<Instance>:MSReport:LMQuantity

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:MSReport:WMQuantity
	CONFigure:GSM:SIGNaling<Instance>:MSReport:LMQuantity



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.MsReport.MsReport
	:members:
	:undoc-members:
	:noindex: