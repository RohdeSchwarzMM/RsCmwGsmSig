Band
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:BAND:BCCH

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:BAND:BCCH



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Band.Band
	:members:
	:undoc-members:
	:noindex: