EmReport
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:RREPort:CSWitched:EMReport:ENABle

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:RREPort:CSWitched:EMReport:ENABle



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Rreport_.Cswitched_.EmReport.EmReport
	:members:
	:undoc-members:
	:noindex: