State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:SIGNaling<Instance>:CSWitched:STATe

.. code-block:: python

	FETCh:GSM:SIGNaling<Instance>:CSWitched:STATe



.. autoclass:: RsCmwGsmSig.Implementations.Cswitched_.State.State
	:members:
	:undoc-members:
	:noindex: