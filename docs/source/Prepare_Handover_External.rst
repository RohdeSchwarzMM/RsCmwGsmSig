External
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:EXTernal:DESTination
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:EXTernal:LTE
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:EXTernal:GSM
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:EXTernal:CDMA
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:EXTernal:EVDO
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:EXTernal:WCDMa
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:EXTernal:TDSCdma

.. code-block:: python

	PREPare:GSM:SIGNaling<Instance>:HANDover:EXTernal:DESTination
	PREPare:GSM:SIGNaling<Instance>:HANDover:EXTernal:LTE
	PREPare:GSM:SIGNaling<Instance>:HANDover:EXTernal:GSM
	PREPare:GSM:SIGNaling<Instance>:HANDover:EXTernal:CDMA
	PREPare:GSM:SIGNaling<Instance>:HANDover:EXTernal:EVDO
	PREPare:GSM:SIGNaling<Instance>:HANDover:EXTernal:WCDMa
	PREPare:GSM:SIGNaling<Instance>:HANDover:EXTernal:TDSCdma



.. autoclass:: RsCmwGsmSig.Implementations.Prepare_.Handover_.External.External
	:members:
	:undoc-members:
	:noindex: