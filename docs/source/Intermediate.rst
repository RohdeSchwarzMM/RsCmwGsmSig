Intermediate
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Intermediate.Intermediate
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.intermediate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Intermediate_Ber.rst
	Intermediate_Bler.rst