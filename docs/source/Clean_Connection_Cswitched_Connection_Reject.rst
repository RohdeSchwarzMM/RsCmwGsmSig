Reject
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CLEan:GSM:SIGNaling<Instance>:CONNection:CSWitched:CONNection:REJect

.. code-block:: python

	CLEan:GSM:SIGNaling<Instance>:CONNection:CSWitched:CONNection:REJect



.. autoclass:: RsCmwGsmSig.Implementations.Clean_.Connection_.Cswitched_.Connection_.Reject.Reject
	:members:
	:undoc-members:
	:noindex: