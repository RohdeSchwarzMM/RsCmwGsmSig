Pswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:PDPContext
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:TAVGtw
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:BPERiod
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:PCMChannel
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:CREQuest
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:NEUTbf
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:EUNodata
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:IARTimer
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:TRTimer

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:PDPContext
	CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:TAVGtw
	CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:BPERiod
	CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:PCMChannel
	CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:CREQuest
	CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:NEUTbf
	CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:EUNodata
	CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:IARTimer
	CONFigure:GSM:SIGNaling<Instance>:CELL:PSWitched:TRTimer



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cell_.Pswitched.Pswitched
	:members:
	:undoc-members:
	:noindex: