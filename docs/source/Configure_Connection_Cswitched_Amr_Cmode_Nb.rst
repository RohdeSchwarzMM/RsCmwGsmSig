Nb
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Cswitched_.Amr_.Cmode_.Nb.Nb
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.cswitched.amr.cmode.nb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Cswitched_Amr_Cmode_Nb_Frate.rst
	Configure_Connection_Cswitched_Amr_Cmode_Nb_Hrate.rst