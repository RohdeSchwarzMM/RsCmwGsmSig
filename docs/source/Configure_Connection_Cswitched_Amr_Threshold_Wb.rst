Wb
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Cswitched_.Amr_.Threshold_.Wb.Wb
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.cswitched.amr.threshold.wb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Cswitched_Amr_Threshold_Wb_Frate.rst
	Configure_Connection_Cswitched_Amr_Threshold_Wb_Hrate.rst