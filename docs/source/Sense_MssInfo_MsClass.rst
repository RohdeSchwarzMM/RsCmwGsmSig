MsClass
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:MSCLass:GPRS
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:MSCLass:EGPRs
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:MSCLass:DGPRs
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:MSCLass:DEGPrs

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:MSSinfo:MSCLass:GPRS
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:MSCLass:EGPRs
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:MSCLass:DGPRs
	SENSe:GSM:SIGNaling<Instance>:MSSinfo:MSCLass:DEGPrs



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.MssInfo_.MsClass.MsClass
	:members:
	:undoc-members:
	:noindex: