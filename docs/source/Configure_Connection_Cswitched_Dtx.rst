Dtx
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:DTX:DL

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:DTX:DL



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Cswitched_.Dtx.Dtx
	:members:
	:undoc-members:
	:noindex: