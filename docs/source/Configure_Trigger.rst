Trigger
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:TRIGger:FTMode

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:TRIGger:FTMode



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Trigger.Trigger
	:members:
	:undoc-members:
	:noindex: