Atimeout
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:ATIMeout:MTC
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:ATIMeout:MOC

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CELL:ATIMeout:MTC
	CONFigure:GSM:SIGNaling<Instance>:CELL:ATIMeout:MOC



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cell_.Atimeout.Atimeout
	:members:
	:undoc-members:
	:noindex: