Mbep
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:RREPort:CSWitched:MBEP:RANGe
	single: SENSe:GSM:SIGNaling<Instance>:RREPort:CSWitched:MBEP

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:RREPort:CSWitched:MBEP:RANGe
	SENSe:GSM:SIGNaling<Instance>:RREPort:CSWitched:MBEP



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Rreport_.Cswitched_.Mbep.Mbep
	:members:
	:undoc-members:
	:noindex: