Flexible
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:GSM:SIGNaling<Instance>:SCENario:SCFDiversity:FLEXible:EXTernal
	single: ROUTe:GSM:SIGNaling<Instance>:SCENario:SCFDiversity:FLEXible:INTernal

.. code-block:: python

	ROUTe:GSM:SIGNaling<Instance>:SCENario:SCFDiversity:FLEXible:EXTernal
	ROUTe:GSM:SIGNaling<Instance>:SCENario:SCFDiversity:FLEXible:INTernal



.. autoclass:: RsCmwGsmSig.Implementations.Route_.Scenario_.ScfDiversity_.Flexible.Flexible
	:members:
	:undoc-members:
	:noindex: