Iloss
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:MODE
	single: CONFigure:GSM:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:CSAMples

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:MODE
	CONFigure:GSM:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:CSAMples



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Fading_.Fsimulator_.Iloss.Iloss
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.fsimulator.iloss.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Fsimulator_Iloss_Loss.rst