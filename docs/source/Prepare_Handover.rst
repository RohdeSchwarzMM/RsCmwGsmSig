Handover
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:DESTination
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:MMODe
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:TARGet
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:PCL
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:TSLot

.. code-block:: python

	PREPare:GSM:SIGNaling<Instance>:HANDover:DESTination
	PREPare:GSM:SIGNaling<Instance>:HANDover:MMODe
	PREPare:GSM:SIGNaling<Instance>:HANDover:TARGet
	PREPare:GSM:SIGNaling<Instance>:HANDover:PCL
	PREPare:GSM:SIGNaling<Instance>:HANDover:TSLot



.. autoclass:: RsCmwGsmSig.Implementations.Prepare_.Handover.Handover
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prepare.handover.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prepare_Handover_Catalog.rst
	Prepare_Handover_Channel.rst
	Prepare_Handover_Level.rst
	Prepare_Handover_Pswitched.rst
	Prepare_Handover_External.rst