Wcdma
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Rreport_.Ncell_.Wcdma.Wcdma
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.rreport.ncell.wcdma.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Rreport_Ncell_Wcdma_Cell.rst