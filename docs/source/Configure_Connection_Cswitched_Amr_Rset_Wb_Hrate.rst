Hrate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:RSET:WB:HRATe:EPSK

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:RSET:WB:HRATe:EPSK



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Cswitched_.Amr_.Rset_.Wb_.Hrate.Hrate
	:members:
	:undoc-members:
	:noindex: