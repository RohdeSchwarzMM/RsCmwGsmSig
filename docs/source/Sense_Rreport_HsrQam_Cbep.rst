Cbep
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:RREPort:HSRQam<HsrQAM>:CBEP

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:RREPort:HSRQam<HsrQAM>:CBEP



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Rreport_.HsrQam_.Cbep.Cbep
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.rreport.hsrQam.cbep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Rreport_HsrQam_Cbep_Range.rst