Cbep
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:RREPort:NSRQam<NsrQAM>:CBEP

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:RREPort:NSRQam<NsrQAM>:CBEP



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Rreport_.Nsrqam_.Cbep.Cbep
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.rreport.nsrqam.cbep.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Rreport_Nsrqam_Cbep_Range.rst