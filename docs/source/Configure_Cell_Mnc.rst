Mnc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:MNC:DIGits
	single: CONFigure:GSM:SIGNaling<Instance>:CELL:MNC

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CELL:MNC:DIGits
	CONFigure:GSM:SIGNaling<Instance>:CELL:MNC



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Cell_.Mnc.Mnc
	:members:
	:undoc-members:
	:noindex: