UdCycle
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Pswitched_.Sconfig_.UdCycle.UdCycle
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pswitched.sconfig.udCycle.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pswitched_Sconfig_UdCycle_Downlink.rst