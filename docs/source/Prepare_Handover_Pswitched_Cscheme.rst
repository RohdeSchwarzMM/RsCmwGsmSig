Cscheme
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:PSWitched:CSCHeme:UL

.. code-block:: python

	PREPare:GSM:SIGNaling<Instance>:HANDover:PSWitched:CSCHeme:UL



.. autoclass:: RsCmwGsmSig.Implementations.Prepare_.Handover_.Pswitched_.Cscheme.Cscheme
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prepare.handover.pswitched.cscheme.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prepare_Handover_Pswitched_Cscheme_Downlink.rst