Scenario
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:GSM:SIGNaling<Instance>:SCENario

.. code-block:: python

	ROUTe:GSM:SIGNaling<Instance>:SCENario



.. autoclass:: RsCmwGsmSig.Implementations.Route_.Scenario.Scenario
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_Scell.rst
	Route_Scenario_Iori.rst
	Route_Scenario_Batch.rst
	Route_Scenario_ScFading.rst
	Route_Scenario_ScfDiversity.rst