Cswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:RREPort:CSWitched:NRBLocks

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:RREPort:CSWitched:NRBLocks



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.Rreport_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.rreport.cswitched.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Rreport_Cswitched_Mbep.rst
	Sense_Rreport_Cswitched_Cbep.rst