Carrier<Carrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.bler.carrier.repcap_carrier_get()
	driver.bler.carrier.repcap_carrier_set(repcap.Carrier.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:SIGNaling<Instance>:BLER:CARRier<Carrier>
	single: READ:GSM:SIGNaling<Instance>:BLER:CARRier<Carrier>

.. code-block:: python

	FETCh:GSM:SIGNaling<Instance>:BLER:CARRier<Carrier>
	READ:GSM:SIGNaling<Instance>:BLER:CARRier<Carrier>



.. autoclass:: RsCmwGsmSig.Implementations.Bler_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.bler.carrier.clone()