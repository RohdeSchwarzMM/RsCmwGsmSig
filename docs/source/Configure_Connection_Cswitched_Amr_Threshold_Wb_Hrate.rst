Hrate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:THReshold:WB:HRATe:EPSK

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:THReshold:WB:HRATe:EPSK



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Cswitched_.Amr_.Threshold_.Wb_.Hrate.Hrate
	:members:
	:undoc-members:
	:noindex: