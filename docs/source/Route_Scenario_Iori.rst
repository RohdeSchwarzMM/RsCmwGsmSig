Iori
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:GSM:SIGNaling<Instance>:SCENario:IORI:FLEXible

.. code-block:: python

	ROUTe:GSM:SIGNaling<Instance>:SCENario:IORI:FLEXible



.. autoclass:: RsCmwGsmSig.Implementations.Route_.Scenario_.Iori.Iori
	:members:
	:undoc-members:
	:noindex: