Ber
----------------------------------------





.. autoclass:: RsCmwGsmSig.Implementations.Intermediate_.Ber.Ber
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.intermediate.ber.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Intermediate_Ber_Cswitched.rst
	Intermediate_Ber_Pswitched.rst