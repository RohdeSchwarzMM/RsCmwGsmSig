Channel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: PREPare:GSM:SIGNaling<Instance>:HANDover:CHANnel:TCH

.. code-block:: python

	PREPare:GSM:SIGNaling<Instance>:HANDover:CHANnel:TCH



.. autoclass:: RsCmwGsmSig.Implementations.Prepare_.Handover_.Channel.Channel
	:members:
	:undoc-members:
	:noindex: