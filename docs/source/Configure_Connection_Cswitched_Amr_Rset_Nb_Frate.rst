Frate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:RSET:NB:FRATe:GMSK

.. code-block:: python

	CONFigure:GSM:SIGNaling<Instance>:CONNection:CSWitched:AMR:RSET:NB:FRATe:GMSK



.. autoclass:: RsCmwGsmSig.Implementations.Configure_.Connection_.Cswitched_.Amr_.Rset_.Nb_.Frate.Frate
	:members:
	:undoc-members:
	:noindex: