Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:AVERage
	single: READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:AVERage

.. code-block:: python

	FETCh:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:AVERage
	READ:GSM:SIGNaling<Instance>:THRoughput:TRACe:UL:PDU:AVERage



.. autoclass:: RsCmwGsmSig.Implementations.Throughput_.Trace_.Uplink_.Pdu_.Average.Average
	:members:
	:undoc-members:
	:noindex: