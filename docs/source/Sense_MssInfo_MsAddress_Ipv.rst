Ipv<IPversion>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: IPv4 .. IPv6
	rc = driver.sense.mssInfo.msAddress.ipv.repcap_iPversion_get()
	driver.sense.mssInfo.msAddress.ipv.repcap_iPversion_set(repcap.IPversion.IPv4)



.. rubric:: SCPI Commands

.. index::
	single: SENSe:GSM:SIGNaling<Instance>:MSSinfo:MSADdress:IPV<IPversion>

.. code-block:: python

	SENSe:GSM:SIGNaling<Instance>:MSSinfo:MSADdress:IPV<IPversion>



.. autoclass:: RsCmwGsmSig.Implementations.Sense_.MssInfo_.MsAddress_.Ipv.Ipv
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.mssInfo.msAddress.ipv.clone()